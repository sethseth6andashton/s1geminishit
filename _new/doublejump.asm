		tst.w	$12(a0) ; is Sonic moving upwards?
		beq.s	endofdoublejump ; if not, branch
		cmpi.b	#$13,$1C(a0) ; is sonic using "bounce" animation?
		beq.w	endofdoublejump ; if yes, branch
		cmpi.b	#2,$1C(a0) ; is sonic jumping/rolling?
		move.b	($FFFFF605).w,d0
		andi.b	#$20,d0 ; is C button pressed?
		beq.s	endofdoublejump ; if not, branch
		sub.w	#$600,$12(a0) ; move sonic upwards
		move.b	#2,$1C(a0) ;use "rolling" animation
		move.w	#$A0,d0
		jsr	(PlaySound_Special).l
		move.b	#1,$21(a0) ; set the double jump flag
endofdoublejump:
		rts