; --------------------------------------------------------------------------------
; Sprite mappings - output from SonMapEd - Sonic 1 format
; --------------------------------------------------------------------------------

SME_uSklS:	
		dc.w SME_uSklS_10-SME_uSklS, SME_uSklS_11-SME_uSklS	
		dc.w SME_uSklS_26-SME_uSklS, SME_uSklS_3B-SME_uSklS	
		dc.w SME_uSklS_50-SME_uSklS, SME_uSklS_65-SME_uSklS	
		dc.w SME_uSklS_7A-SME_uSklS, SME_uSklS_8F-SME_uSklS	
SME_uSklS_10:	dc.b 0	
SME_uSklS_11:	dc.b 4	
		dc.b $E8, $A, 0, 0, $E8	
		dc.b $E8, $A, 0, 9, 0	
		dc.b 0, $A, $10, 0, $E8	
		dc.b 0, $A, $10, 9, 0	
SME_uSklS_26:	dc.b 4	
		dc.b $E8, $A, 8, $12, $E9	
		dc.b $E8, $A, 0, $12, 0	
		dc.b 0, $A, $18, $12, $E9	
		dc.b 0, $A, $10, $12, 0	
SME_uSklS_3B:	dc.b 4	
		dc.b $E8, $A, 8, 9, $E8	
		dc.b $E8, $A, 8, 0, 0	
		dc.b 0, $A, $18, 9, $E8	
		dc.b 0, $A, $18, 0, 0	
SME_uSklS_50:	dc.b 4	
		dc.b $E8, $A, 0, 0, $E8	
		dc.b $E8, $A, 0, 9, 0	
		dc.b 0, $A, $18, 9, $E8	
		dc.b 0, $A, $18, 0, 0	
SME_uSklS_65:	dc.b 4	
		dc.b $E8, $A, 8, 9, $E8	
		dc.b $E8, $A, 8, 0, 0	
		dc.b 0, $A, $10, 0, $E8	
		dc.b 0, $A, $10, 9, 0	
SME_uSklS_7A:	dc.b 4	
		dc.b $E8, $A, 0, $12, $E8	
		dc.b $E8, $A, 0, $1B, 0	
		dc.b 0, $A, $18, $1B, $E8	
		dc.b 0, $A, $18, $12, 0	
SME_uSklS_8F:	dc.b 4	
		dc.b $E8, $A, 8, $1B, $E8	
		dc.b $E8, $A, 8, $12, 0	
		dc.b 0, $A, $10, $12, $E8	
		dc.b 0, $A, $10, $1B, 0	
		even