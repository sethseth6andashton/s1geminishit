; --------------------------------------------------------------------------------
; Sprite mappings - output from SonMapEd - Sonic 1 format
; --------------------------------------------------------------------------------

SME_De_1M:	
		dc.w SME_De_1M_DC-SME_De_1M, SME_De_1M_DD-SME_De_1M	
		dc.w SME_De_1M_F2-SME_De_1M, SME_De_1M_102-SME_De_1M	
		dc.w SME_De_1M_112-SME_De_1M, SME_De_1M_122-SME_De_1M	
		dc.w SME_De_1M_132-SME_De_1M, SME_De_1M_147-SME_De_1M	
		dc.w SME_De_1M_152-SME_De_1M, SME_De_1M_15D-SME_De_1M	
		dc.w SME_De_1M_172-SME_De_1M, SME_De_1M_17D-SME_De_1M	
		dc.w SME_De_1M_18D-SME_De_1M, SME_De_1M_1A7-SME_De_1M	
		dc.w SME_De_1M_1C6-SME_De_1M, SME_De_1M_1DB-SME_De_1M	
		dc.w SME_De_1M_1F5-SME_De_1M, SME_De_1M_20A-SME_De_1M	
		dc.w SME_De_1M_224-SME_De_1M, SME_De_1M_239-SME_De_1M	
		dc.w SME_De_1M_244-SME_De_1M, SME_De_1M_24F-SME_De_1M	
		dc.w SME_De_1M_264-SME_De_1M, SME_De_1M_26F-SME_De_1M	
		dc.w SME_De_1M_27F-SME_De_1M, SME_De_1M_29E-SME_De_1M	
		dc.w SME_De_1M_2BD-SME_De_1M, SME_De_1M_2D2-SME_De_1M	
		dc.w SME_De_1M_2EC-SME_De_1M, SME_De_1M_301-SME_De_1M	
		dc.w SME_De_1M_31B-SME_De_1M, SME_De_1M_326-SME_De_1M	
		dc.w SME_De_1M_331-SME_De_1M, SME_De_1M_33C-SME_De_1M	
		dc.w SME_De_1M_347-SME_De_1M, SME_De_1M_35C-SME_De_1M	
		dc.w SME_De_1M_36C-SME_De_1M, SME_De_1M_381-SME_De_1M	
		dc.w SME_De_1M_391-SME_De_1M, SME_De_1M_39C-SME_De_1M	
		dc.w SME_De_1M_3A7-SME_De_1M, SME_De_1M_3B2-SME_De_1M	
		dc.w SME_De_1M_3BD-SME_De_1M, SME_De_1M_3D2-SME_De_1M	
		dc.w SME_De_1M_3DD-SME_De_1M, SME_De_1M_3F2-SME_De_1M	
		dc.w SME_De_1M_3FD-SME_De_1M, SME_De_1M_403-SME_De_1M	
		dc.w SME_De_1M_409-SME_De_1M, SME_De_1M_40F-SME_De_1M	
		dc.w SME_De_1M_415-SME_De_1M, SME_De_1M_41B-SME_De_1M	
		dc.w SME_De_1M_426-SME_De_1M, SME_De_1M_42C-SME_De_1M	
		dc.w SME_De_1M_437-SME_De_1M, SME_De_1M_43D-SME_De_1M	
		dc.w SME_De_1M_448-SME_De_1M, SME_De_1M_45D-SME_De_1M	
		dc.w SME_De_1M_472-SME_De_1M, SME_De_1M_482-SME_De_1M	
		dc.w SME_De_1M_492-SME_De_1M, SME_De_1M_4A2-SME_De_1M	
		dc.w SME_De_1M_4AD-SME_De_1M, SME_De_1M_4BD-SME_De_1M	
		dc.w SME_De_1M_4CD-SME_De_1M, SME_De_1M_4DD-SME_De_1M	
		dc.w SME_De_1M_4F2-SME_De_1M, SME_De_1M_507-SME_De_1M	
		dc.w SME_De_1M_521-SME_De_1M, SME_De_1M_53B-SME_De_1M	
		dc.w SME_De_1M_546-SME_De_1M, SME_De_1M_556-SME_De_1M	
		dc.w SME_De_1M_561-SME_De_1M, SME_De_1M_571-SME_De_1M	
		dc.w SME_De_1M_57C-SME_De_1M, SME_De_1M_58C-SME_De_1M	
		dc.w SME_De_1M_59C-SME_De_1M, SME_De_1M_5B6-SME_De_1M	
		dc.w SME_De_1M_5D0-SME_De_1M, SME_De_1M_5DB-SME_De_1M	
		dc.w SME_De_1M_5EB-SME_De_1M, SME_De_1M_5F1-SME_De_1M	
		dc.w SME_De_1M_5F7-SME_De_1M, SME_De_1M_5FD-SME_De_1M	
		dc.w SME_De_1M_60D-SME_De_1M, SME_De_1M_61D-SME_De_1M	
		dc.w SME_De_1M_62D-SME_De_1M, SME_De_1M_63D-SME_De_1M	
		dc.w SME_De_1M_648-SME_De_1M, SME_De_1M_65D-SME_De_1M	
		dc.w SME_De_1M_672-SME_De_1M, SME_De_1M_687-SME_De_1M	
		dc.w SME_De_1M_69C-SME_De_1M, SME_De_1M_6B1-SME_De_1M	
		dc.w SME_De_1M_6C6-SME_De_1M, SME_De_1M_6EA-SME_De_1M	
		dc.w SME_De_1M_70E-SME_De_1M, SME_De_1M_732-SME_De_1M	
		dc.w SME_De_1M_751-SME_De_1M, SME_De_1M_77A-SME_De_1M	
		dc.w SME_De_1M_79E-SME_De_1M, SME_De_1M_7CC-SME_De_1M	
		dc.w SME_De_1M_7F0-SME_De_1M, SME_De_1M_80A-SME_De_1M	
		dc.w SME_De_1M_824-SME_De_1M, SME_De_1M_83E-SME_De_1M	
		dc.w SME_De_1M_858-SME_De_1M, SME_De_1M_881-SME_De_1M	
		dc.w SME_De_1M_8AF-SME_De_1M, SME_De_1M_8DD-SME_De_1M	
SME_De_1M_DC:	dc.b 0	
SME_De_1M_DD:	dc.b 4	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $D, 0, 3, $F0	
		dc.b 4, 8, 0, $B, $F0	
		dc.b $C, 8, 0, $E, $F8	
SME_De_1M_F2:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, 9, 0, 6, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_De_1M_102:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, 9, 0, 6, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_De_1M_112:	dc.b 3	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, 9, 0, 6, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_De_1M_122:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F0	
		dc.b 4, 8, 0, 9, $F0	
		dc.b $C, 8, 0, $C, $F8	
SME_De_1M_132:	dc.b 4	
		dc.b $EB, $D, 0, 0, $EC	
		dc.b $FB, 9, 0, 8, $EC	
		dc.b $FB, 6, 0, $E, 4	
		dc.b $B, 4, 0, $14, $EC	
SME_De_1M_147:	dc.b 2	
		dc.b $EC, $D, 0, 0, $ED	
		dc.b $FC, $E, 0, 8, $F5	
SME_De_1M_152:	dc.b 2	
		dc.b $ED, 9, 0, 0, $F3	
		dc.b $FD, $A, 0, 6, $F3	
SME_De_1M_15D:	dc.b 4	
		dc.b $EB, 9, 0, 0, $F4	
		dc.b $FB, 9, 0, 6, $EC	
		dc.b $FB, 6, 0, $C, 4	
		dc.b $B, 4, 0, $12, $EC	
SME_De_1M_172:	dc.b 2	
		dc.b $EC, 9, 0, 0, $F3	
		dc.b $FC, $E, 0, 6, $EB	
SME_De_1M_17D:	dc.b 3	
		dc.b $ED, $D, 0, 0, $EC	
		dc.b $FD, $C, 0, 8, $F4	
		dc.b 5, 9, 0, $C, $F4	
SME_De_1M_18D:	dc.b 5	
		dc.b $EB, 9, 0, 0, $EB	
		dc.b $EB, 6, 0, 6, 3	
		dc.b $FB, 8, 0, $C, $EB	
		dc.b 3, 9, 0, $F, $F3	
		dc.b $13, 0, 0, $15, $FB	
SME_De_1M_1A7:	dc.b 6	
		dc.b $EC, 9, 0, 0, $EC	
		dc.b $EC, 1, 0, 6, 4	
		dc.b $FC, $C, 0, 8, $EC	
		dc.b 4, 9, 0, $C, $F4	
		dc.b $FC, 5, 0, $12, $C	
		dc.b $F4, 0, 0, $16, $14	
SME_De_1M_1C6:	dc.b 4	
		dc.b $ED, 9, 0, 0, $ED	
		dc.b $ED, 1, 0, 6, 5	
		dc.b $FD, $D, 0, 8, $F5	
		dc.b $D, 8, 0, $10, $FD	
SME_De_1M_1DB:	dc.b 5	
		dc.b $EB, 9, 0, 0, $EB	
		dc.b $EB, 5, 0, 6, 3	
		dc.b $FB, $D, 0, $A, $F3	
		dc.b $B, 8, 0, $12, $F3	
		dc.b $13, 4, 0, $15, $FB	
SME_De_1M_1F5:	dc.b 4	
		dc.b $EC, 9, 0, 0, $EC	
		dc.b $EC, 1, 0, 6, 4	
		dc.b $FC, $D, 0, 8, $F4	
		dc.b $C, 8, 0, $10, $FC	
SME_De_1M_20A:	dc.b 5	
		dc.b $ED, 9, 0, 0, $ED	
		dc.b $ED, 1, 0, 6, 5	
		dc.b $FD, 0, 0, 8, $ED	
		dc.b $FD, $D, 0, 9, $F5	
		dc.b $D, 8, 0, $11, $FD	
SME_De_1M_224:	dc.b 4	
		dc.b $F4, 7, 0, 0, $EB	
		dc.b $EC, 9, 0, 8, $FB	
		dc.b $FC, 4, 0, $E, $FB	
		dc.b 4, 9, 0, $10, $FB	
SME_De_1M_239:	dc.b 2	
		dc.b $F4, 7, 0, 0, $EC	
		dc.b $EC, $B, 0, 8, $FC	
SME_De_1M_244:	dc.b 2	
		dc.b $F4, 6, 0, 0, $ED	
		dc.b $F4, $A, 0, 6, $FD	
SME_De_1M_24F:	dc.b 4	
		dc.b $F4, 6, 0, 0, $EB	
		dc.b $EC, 9, 0, 6, $FB	
		dc.b $FC, 4, 0, $C, $FB	
		dc.b 4, 9, 0, $E, $FB	
SME_De_1M_264:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EC	
		dc.b $F4, $B, 0, 6, $FC	
SME_De_1M_26F:	dc.b 3	
		dc.b $F4, 7, 0, 0, $ED	
		dc.b $EC, 0, 0, 8, $FD	
		dc.b $F4, $A, 0, 9, $FD	
SME_De_1M_27F:	dc.b 6	
		dc.b $FD, 6, 0, 0, $EB	
		dc.b $ED, 4, 0, 6, $F3	
		dc.b $F5, 4, 0, 8, $EB	
		dc.b $F5, $A, 0, $A, $FB	
		dc.b $D, 0, 0, $13, $FB	
		dc.b $FD, 0, 0, $14, $13	
SME_De_1M_29E:	dc.b 6	
		dc.b $FC, 6, 0, 0, $EC	
		dc.b $E4, 8, 0, 6, $F4	
		dc.b $EC, 4, 0, 9, $FC	
		dc.b $F4, 4, 0, $B, $EC	
		dc.b $F4, $A, 0, $D, $FC	
		dc.b $C, 0, 0, $16, $FC	
SME_De_1M_2BD:	dc.b 4	
		dc.b $FB, 6, 0, 0, $ED	
		dc.b $F3, 4, 0, 6, $ED	
		dc.b $EB, $A, 0, 8, $FD	
		dc.b 3, 4, 0, $11, $FD	
SME_De_1M_2D2:	dc.b 5	
		dc.b $FD, 6, 0, 0, $EB	
		dc.b $ED, 8, 0, 6, $F3	
		dc.b $F5, 4, 0, 9, $EB	
		dc.b $F5, $D, 0, $B, $FB	
		dc.b 5, 8, 0, $13, $FB	
SME_De_1M_2EC:	dc.b 4	
		dc.b $FC, 6, 0, 0, $EC	
		dc.b $F4, 4, 0, 6, $EC	
		dc.b $EC, $A, 0, 8, $FC	
		dc.b 4, 4, 0, $11, $FC	
SME_De_1M_301:	dc.b 5	
		dc.b $FB, 6, 0, 0, $ED	
		dc.b $EB, $A, 0, 6, $FD	
		dc.b $F3, 4, 0, $F, $ED	
		dc.b 3, 4, 0, $11, $FD	
		dc.b $B, 0, 0, $13, $FD	
SME_De_1M_31B:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_De_1M_326:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_De_1M_331:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_De_1M_33C:	dc.b 2	
		dc.b $EE, 9, 0, 0, $F4	
		dc.b $FE, $E, 0, 6, $EC	
SME_De_1M_347:	dc.b 4	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
		dc.b $FE, 0, 0, $14, $EE	
SME_De_1M_35C:	dc.b 3	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
SME_De_1M_36C:	dc.b 4	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
		dc.b $FE, 0, 0, $14, $EE	
SME_De_1M_381:	dc.b 3	
		dc.b $EE, 9, 0, 0, $EE	
		dc.b $EE, 1, 0, 6, 6	
		dc.b $FE, $E, 0, 8, $F6	
SME_De_1M_391:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_De_1M_39C:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_De_1M_3A7:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_De_1M_3B2:	dc.b 2	
		dc.b $F4, 6, 0, 0, $EE	
		dc.b $F4, $B, 0, 6, $FE	
SME_De_1M_3BD:	dc.b 4	
		dc.b $FA, 6, 0, 0, $EE	
		dc.b $F2, 4, 0, 6, $EE	
		dc.b $EA, $B, 0, 8, $FE	
		dc.b $A, 0, 0, $14, $FE	
SME_De_1M_3D2:	dc.b 2	
		dc.b $F2, 7, 0, 0, $EE	
		dc.b $EA, $B, 0, 8, $FE	
SME_De_1M_3DD:	dc.b 4	
		dc.b $FA, 6, 0, 0, $EE	
		dc.b $F2, 4, 0, 6, $EE	
		dc.b $EA, $B, 0, 8, $FE	
		dc.b $A, 0, 0, $14, $FE	
SME_De_1M_3F2:	dc.b 2	
		dc.b $F2, 7, 0, 0, $EE	
		dc.b $EA, $B, 0, 8, $FE	
SME_De_1M_3FD:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_403:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_409:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_40F:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_415:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_41B:	dc.b 2	
		dc.b $F4, $E, 0, 0, $EC	
		dc.b $F4, 2, 0, $C, $C	
SME_De_1M_426:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_42C:	dc.b 2	
		dc.b $EC, $B, 0, 0, $F4	
		dc.b $C, 8, 0, $C, $F4	
SME_De_1M_437:	dc.b 1	
		dc.b $F0, $F, 0, 0, $F0	
SME_De_1M_43D:	dc.b 2	
		dc.b $ED, 9, 0, 0, $F0	
		dc.b $FD, $E, 0, 6, $F0	
SME_De_1M_448:	dc.b 4	
		dc.b $ED, 9, 0, 0, $F0	
		dc.b $FD, $D, 0, 6, $F0	
		dc.b $D, 4, 0, $E, 0	
		dc.b 5, 0, 0, $10, $E8	
SME_De_1M_45D:	dc.b 4	
		dc.b $F4, 4, 0, 0, $FC	
		dc.b $FC, $D, 0, 2, $F4	
		dc.b $C, 8, 0, $A, $F4	
		dc.b 4, 0, 0, $D, $EC	
SME_De_1M_472:	dc.b 3	
		dc.b $EC, 8, 8, 0, $E8	
		dc.b $F4, 2, 8, 3, 0	
		dc.b $F4, $F, 8, 6, $E0	
SME_De_1M_482:	dc.b 3	
		dc.b $EC, $E, 8, 0, $E8	
		dc.b 4, $D, 8, $C, $E0	
		dc.b $C, 0, $18, $14, 0	
SME_De_1M_492:	dc.b 3	
		dc.b $F4, $D, 0, 0, $FC	
		dc.b $FC, 5, 0, 8, $EC	
		dc.b 4, 8, 0, $C, $FC	
SME_De_1M_4A2:	dc.b 2	
		dc.b $F4, $A, 0, 0, $E8	
		dc.b $F4, $A, 8, 0, 0	
SME_De_1M_4AD:	dc.b 3	
		dc.b $F4, $D, 0, 0, $E4	
		dc.b $FC, 0, 0, 8, 4	
		dc.b 4, $C, 0, 9, $EC	
SME_De_1M_4BD:	dc.b 3	
		dc.b $F4, $D, 0, 0, $FC	
		dc.b $FC, 5, 0, 8, $EC	
		dc.b 4, 8, 0, $C, $FC	
SME_De_1M_4CD:	dc.b 3	
		dc.b $E8, $B, 0, 0, $F0	
		dc.b 8, 4, 0, $C, $F8	
		dc.b $10, 0, 0, $E, $F8	
SME_De_1M_4DD:	dc.b 4	
		dc.b $F8, $E, 0, 0, $E8	
		dc.b 0, 5, 0, $C, 8	
		dc.b $F8, 0, 0, $10, 8	
		dc.b $F0, 0, 0, $11, $F8	
SME_De_1M_4F2:	dc.b 4	
		dc.b $F8, $E, 0, 0, $E8	
		dc.b 0, 5, 0, $C, 8	
		dc.b $F8, 0, 0, $10, 8	
		dc.b $F0, 0, 0, $11, $F8	
SME_De_1M_507:	dc.b 5	
		dc.b $E8, $A, 0, 0, $F4	
		dc.b $F0, 1, 0, 9, $C	
		dc.b 0, 9, 0, $B, $F4	
		dc.b $10, 4, 0, $11, $F4	
		dc.b 0, 0, 0, $13, $EC	
SME_De_1M_521:	dc.b 5	
		dc.b $E8, $A, 0, 0, $F4	
		dc.b $E8, 1, 0, 9, $C	
		dc.b 0, 9, 0, $B, $F4	
		dc.b $10, 4, 0, $11, $F4	
		dc.b 0, 0, 0, $13, $EC	
SME_De_1M_53B:	dc.b 2	
		dc.b $ED, $A, 0, 0, $F3	
		dc.b 5, $D, 0, 9, $EB	
SME_De_1M_546:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F3	
		dc.b 4, 8, 0, 9, $F3	
		dc.b $C, 4, 0, $C, $F3	
SME_De_1M_556:	dc.b 2	
		dc.b $ED, $A, 0, 0, $F3	
		dc.b 5, $D, 0, 9, $EB	
SME_De_1M_561:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F3	
		dc.b 4, 8, 0, 9, $F3	
		dc.b $C, 4, 0, $C, $F3	
SME_De_1M_571:	dc.b 2	
		dc.b $EC, 9, 0, 0, $F0	
		dc.b $FC, $E, 0, 6, $F0	
SME_De_1M_57C:	dc.b 3	
		dc.b $EC, $A, 0, 0, $F0	
		dc.b 4, 5, 0, 9, $F8	
		dc.b $E4, 0, 0, $D, $F8	
SME_De_1M_58C:	dc.b 3	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, $B, 0, $A, $F4	
SME_De_1M_59C:	dc.b 5	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, 9, 0, $A, $F4	
		dc.b 8, $C, 0, $10, $F4	
		dc.b $10, 0, 0, $14, $F4	
SME_De_1M_5B6:	dc.b 5	
		dc.b $E8, $D, 0, 0, $EC	
		dc.b $E8, 1, 0, 8, $C	
		dc.b $F8, 9, 0, $A, $F4	
		dc.b 8, $C, 0, $10, $F4	
		dc.b $10, 0, 0, $14, $F4	
SME_De_1M_5D0:	dc.b 2	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $F, 0, 3, $F0	
SME_De_1M_5DB:	dc.b 3	
		dc.b $EC, 8, 0, 0, $F0	
		dc.b $F4, $E, 0, 3, $F0	
		dc.b $C, 8, 0, $F, $F8	
SME_De_1M_5EB:	dc.b 1	
		dc.b $F0, $B, 0, 0, $F4	
SME_De_1M_5F1:	dc.b 1	
		dc.b $F4, 6, 0, 0, $F8	
SME_De_1M_5F7:	dc.b 1	
		dc.b $F8, 1, 0, 0, $FC	
SME_De_1M_5FD:	dc.b 3	
		dc.b $F4, $D, 8, 0, $E4	
		dc.b $FC, 5, 8, 8, 4	
		dc.b 4, 8, 8, $C, $EC	
SME_De_1M_60D:	dc.b 3	
		dc.b $F4, $D, 8, 0, $FC	
		dc.b $FC, 0, 8, 8, $F4	
		dc.b 4, $C, 8, 9, $F4	
SME_De_1M_61D:	dc.b 3	
		dc.b $F0, $E, 0, 0, $EC	
		dc.b $F8, 1, 0, $C, $C	
		dc.b 8, $C, 0, $E, $F4	
SME_De_1M_62D:	dc.b 3	
		dc.b $EB, 9, 0, 0, $F4	
		dc.b $FB, $E, 0, 6, $EC	
		dc.b 3, 1, 0, $12, $C	
SME_De_1M_63D:	dc.b 2	
		dc.b $F0, $F, 0, 0, $EC	
		dc.b $F8, 2, 0, $10, $C	
SME_De_1M_648:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_65D:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_672:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_687:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_69C:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_6B1:	dc.b 4	
		dc.b $C, 8, 0, 0, $F4	
		dc.b 4, $C, 0, 3, $F4	
		dc.b $FC, $C, 0, 7, $F4	
		dc.b $F4, 8, 0, $B, $FC	
SME_De_1M_6C6:	dc.b 7	
		dc.b 8, $C, 0, 0, $EC	
		dc.b 8, 0, 0, 4, $C	
		dc.b 0, $C, 0, 5, $EC	
		dc.b 0, 0, 0, 9, $C	
		dc.b $F8, $C, 0, $A, $EC	
		dc.b $F8, 0, 0, $E, $C	
		dc.b $F0, $C, 0, $F, $F4	
SME_De_1M_6EA:	dc.b 7	
		dc.b 8, $C, 0, 0, $EC	
		dc.b 8, 0, 0, 4, $C	
		dc.b 0, $C, 0, 5, $EC	
		dc.b 0, 0, 0, 9, $C	
		dc.b $F8, $C, 0, $A, $EC	
		dc.b $F8, 0, 0, $E, $C	
		dc.b $F0, $C, 0, $F, $F4	
SME_De_1M_70E:	dc.b 7	
		dc.b 8, $C, 0, 0, $EC	
		dc.b 8, 0, 0, 4, $C	
		dc.b 0, $C, 0, 5, $EC	
		dc.b 0, 0, 0, 9, $C	
		dc.b $F8, $C, 0, $A, $EC	
		dc.b $F8, 0, 0, $E, $C	
		dc.b $F0, $C, 0, $F, $F4	
SME_De_1M_732:	dc.b 6	
		dc.b 8, $C, 0, 0, $F5	
		dc.b 0, $C, 0, 4, $ED	
		dc.b 0, 0, 0, 8, $D	
		dc.b $F8, $C, 0, 9, $ED	
		dc.b $F8, 0, 0, $D, $D	
		dc.b $F0, $C, 0, $E, $F5	
SME_De_1M_751:	dc.b 8	
		dc.b 9, 8, 0, 0, $ED	
		dc.b 1, $C, 0, 3, $ED	
		dc.b 1, 0, 0, 7, $D	
		dc.b $F9, $C, 0, 8, $ED	
		dc.b $F9, 0, 0, $C, $D	
		dc.b $F1, $C, 0, $D, $ED	
		dc.b $F1, 0, 0, $11, $D	
		dc.b $E9, 8, 0, $12, $F5	
SME_De_1M_77A:	dc.b 7	
		dc.b 8, $C, 0, 0, $ED	
		dc.b 0, $C, 0, 4, $ED	
		dc.b 0, 0, 0, 8, $D	
		dc.b $F8, $C, 0, 9, $ED	
		dc.b $F8, 0, 0, $D, $D	
		dc.b $F0, $C, 0, $E, $ED	
		dc.b $E8, $C, 0, $12, $ED	
SME_De_1M_79E:	dc.b 9	
		dc.b $B, 8, 0, 0, $ED	
		dc.b 3, $C, 0, 3, $ED	
		dc.b 3, 0, 0, 7, $D	
		dc.b $FB, $C, 0, 8, $ED	
		dc.b $FB, 0, 0, $C, $D	
		dc.b $F3, $C, 0, $D, $ED	
		dc.b $F3, 0, 0, $11, $D	
		dc.b $EB, $C, 0, $12, $ED	
		dc.b $E3, 0, 0, $16, $FD	
SME_De_1M_7CC:	dc.b 7	
		dc.b 7, $C, 0, 0, $ED	
		dc.b $FF, $C, 0, 4, $ED	
		dc.b $FF, 0, 0, 8, $D	
		dc.b $F7, $C, 0, 9, $ED	
		dc.b $F7, 0, 0, $D, $D	
		dc.b $EF, $C, 0, $E, $ED	
		dc.b $E7, 8, 0, $12, $F5	
SME_De_1M_7F0:	dc.b 5	
		dc.b 7, 8, 0, 0, $F6	
		dc.b $FF, $C, 0, 3, $EE	
		dc.b $F7, $C, 0, 7, $EE	
		dc.b $EF, $C, 0, $B, $EE	
		dc.b $E7, $C, 0, $F, $EE	
SME_De_1M_80A:	dc.b 5	
		dc.b 7, 8, 0, 0, $F5	
		dc.b $FF, $C, 0, 3, $ED	
		dc.b $F7, $C, 0, 7, $ED	
		dc.b $EF, $C, 0, $B, $ED	
		dc.b $E7, $C, 0, $F, $ED	
SME_De_1M_824:	dc.b 5	
		dc.b 7, 8, 0, 0, $F6	
		dc.b $FF, $C, 0, 3, $EE	
		dc.b $F7, $C, 0, 7, $EE	
		dc.b $EF, $C, 0, $B, $EE	
		dc.b $E7, $C, 0, $F, $EE	
SME_De_1M_83E:	dc.b 5	
		dc.b 6, 4, 0, 0, $F5	
		dc.b $FE, $C, 0, 2, $ED	
		dc.b $F6, $C, 0, 6, $ED	
		dc.b $EE, $C, 0, $A, $ED	
		dc.b $E6, $C, 0, $E, $ED	
SME_De_1M_858:	dc.b 8	
		dc.b 6, $C, 0, 0, $ED	
		dc.b 6, 0, 0, 4, $D	
		dc.b $FE, $C, 0, 5, $E5	
		dc.b $FE, 4, 0, 9, 5	
		dc.b $F6, $C, 0, $B, $E5	
		dc.b $F6, 0, 0, $F, 5	
		dc.b $EE, $C, 0, $10, $E5	
		dc.b $E6, 8, 0, $14, $ED	
SME_De_1M_881:	dc.b 9	
		dc.b 6, $C, 0, 0, $E5	
		dc.b 6, 0, 0, 4, 5	
		dc.b $FE, $C, 0, 5, $E5	
		dc.b $FE, 0, 0, 9, 5	
		dc.b $F6, $C, 0, $A, $E5	
		dc.b $F6, 0, 0, $E, 5	
		dc.b $EE, $C, 0, $F, $E5	
		dc.b $EE, 0, 0, $13, 5	
		dc.b $E6, 4, 0, $14, $F5	
SME_De_1M_8AF:	dc.b 9	
		dc.b 6, $C, 0, 0, $ED	
		dc.b 6, 0, 0, 4, $D	
		dc.b $FE, $C, 0, 5, $E5	
		dc.b $FE, 4, 0, 9, 5	
		dc.b $F6, $C, 0, $B, $E5	
		dc.b $F6, 0, 0, $F, 5	
		dc.b $EE, $C, 0, $10, $E5	
		dc.b $EE, 0, 0, $14, 5	
		dc.b $E6, 8, 0, $15, $ED	
SME_De_1M_8DD:	dc.b 9	
		dc.b 6, $C, 0, 0, $E5	
		dc.b 6, 0, 0, 4, 5	
		dc.b $FE, $C, 0, 5, $E5	
		dc.b $FE, 0, 0, 9, 5	
		dc.b $F6, $C, 0, $A, $E5	
		dc.b $F6, 0, 0, $E, 5	
		dc.b $EE, $C, 0, $F, $E5	
		dc.b $EE, 0, 0, $13, 5	
		dc.b $E6, 4, 0, $14, $F5	
		even