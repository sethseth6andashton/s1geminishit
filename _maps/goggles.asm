; --------------------------------------------------------------------------------
; Sprite mappings - output from SonMapEd - Sonic 1 format
; --------------------------------------------------------------------------------

SME_luklW:	
		dc.w SME_luklW_14-SME_luklW, SME_luklW_15-SME_luklW	
		dc.w SME_luklW_1B-SME_luklW, SME_luklW_21-SME_luklW	
		dc.w SME_luklW_27-SME_luklW, SME_luklW_2D-SME_luklW	
		dc.w SME_luklW_33-SME_luklW, SME_luklW_39-SME_luklW	
		dc.w SME_luklW_3F-SME_luklW, SME_luklW_45-SME_luklW	
SME_luklW_14:	dc.b 0	
SME_luklW_15:	dc.b 1	
		dc.b $E0, 5, $20, 0, $F8	
SME_luklW_1B:	dc.b 1	
		dc.b $E0, 5, $20, 4, $F8	
SME_luklW_21:	dc.b 1	
		dc.b $E0, 5, $20, 8, $F8	
SME_luklW_27:	dc.b 1	
		dc.b $E0, 5, $20, $C, $F8	
SME_luklW_2D:	dc.b 1	
		dc.b $E0, 5, $20, $10, $F8	
SME_luklW_33:	dc.b 1	
		dc.b $E0, 5, $20, $14, $F8	
SME_luklW_39:	dc.b 1	
		dc.b $E0, 5, $20, $18, $F8	
SME_luklW_3F:	dc.b 1	
		dc.b $E0, 5, $20, $1C, $F8	
SME_luklW_45:	dc.b 1	
		dc.b $E0, 5, $20, $20, $F8	
		even