; --------------------------------------------------------------------------------
; Dynamic Pattern Loading Cues - output from SonMapEd - Sonic 1 format; --------------------------------------------------------------------------------

SME_SaFGb:	
		dc.w SME_SaFGb_DC-SME_SaFGb, SME_SaFGb_DD-SME_SaFGb	
		dc.w SME_SaFGb_E6-SME_SaFGb, SME_SaFGb_ED-SME_SaFGb	
		dc.w SME_SaFGb_F4-SME_SaFGb, SME_SaFGb_FB-SME_SaFGb	
		dc.w SME_SaFGb_102-SME_SaFGb, SME_SaFGb_10B-SME_SaFGb	
		dc.w SME_SaFGb_110-SME_SaFGb, SME_SaFGb_115-SME_SaFGb	
		dc.w SME_SaFGb_11E-SME_SaFGb, SME_SaFGb_123-SME_SaFGb	
		dc.w SME_SaFGb_12A-SME_SaFGb, SME_SaFGb_135-SME_SaFGb	
		dc.w SME_SaFGb_142-SME_SaFGb, SME_SaFGb_14B-SME_SaFGb	
		dc.w SME_SaFGb_156-SME_SaFGb, SME_SaFGb_15F-SME_SaFGb	
		dc.w SME_SaFGb_16A-SME_SaFGb, SME_SaFGb_173-SME_SaFGb	
		dc.w SME_SaFGb_178-SME_SaFGb, SME_SaFGb_17D-SME_SaFGb	
		dc.w SME_SaFGb_186-SME_SaFGb, SME_SaFGb_18B-SME_SaFGb	
		dc.w SME_SaFGb_192-SME_SaFGb, SME_SaFGb_19F-SME_SaFGb	
		dc.w SME_SaFGb_1AC-SME_SaFGb, SME_SaFGb_1B5-SME_SaFGb	
		dc.w SME_SaFGb_1C0-SME_SaFGb, SME_SaFGb_1C9-SME_SaFGb	
		dc.w SME_SaFGb_1D4-SME_SaFGb, SME_SaFGb_1D9-SME_SaFGb	
		dc.w SME_SaFGb_1DE-SME_SaFGb, SME_SaFGb_1E3-SME_SaFGb	
		dc.w SME_SaFGb_1E8-SME_SaFGb, SME_SaFGb_1F1-SME_SaFGb	
		dc.w SME_SaFGb_1F8-SME_SaFGb, SME_SaFGb_201-SME_SaFGb	
		dc.w SME_SaFGb_208-SME_SaFGb, SME_SaFGb_20D-SME_SaFGb	
		dc.w SME_SaFGb_212-SME_SaFGb, SME_SaFGb_217-SME_SaFGb	
		dc.w SME_SaFGb_21C-SME_SaFGb, SME_SaFGb_225-SME_SaFGb	
		dc.w SME_SaFGb_22A-SME_SaFGb, SME_SaFGb_233-SME_SaFGb	
		dc.w SME_SaFGb_238-SME_SaFGb, SME_SaFGb_23B-SME_SaFGb	
		dc.w SME_SaFGb_23E-SME_SaFGb, SME_SaFGb_241-SME_SaFGb	
		dc.w SME_SaFGb_244-SME_SaFGb, SME_SaFGb_247-SME_SaFGb	
		dc.w SME_SaFGb_24C-SME_SaFGb, SME_SaFGb_24F-SME_SaFGb	
		dc.w SME_SaFGb_254-SME_SaFGb, SME_SaFGb_257-SME_SaFGb	
		dc.w SME_SaFGb_25C-SME_SaFGb, SME_SaFGb_265-SME_SaFGb	
		dc.w SME_SaFGb_26E-SME_SaFGb, SME_SaFGb_275-SME_SaFGb	
		dc.w SME_SaFGb_27C-SME_SaFGb, SME_SaFGb_283-SME_SaFGb	
		dc.w SME_SaFGb_286-SME_SaFGb, SME_SaFGb_28D-SME_SaFGb	
		dc.w SME_SaFGb_294-SME_SaFGb, SME_SaFGb_29B-SME_SaFGb	
		dc.w SME_SaFGb_2A4-SME_SaFGb, SME_SaFGb_2AD-SME_SaFGb	
		dc.w SME_SaFGb_2B8-SME_SaFGb, SME_SaFGb_2C3-SME_SaFGb	
		dc.w SME_SaFGb_2C8-SME_SaFGb, SME_SaFGb_2CF-SME_SaFGb	
		dc.w SME_SaFGb_2D4-SME_SaFGb, SME_SaFGb_2DB-SME_SaFGb	
		dc.w SME_SaFGb_2E0-SME_SaFGb, SME_SaFGb_2E7-SME_SaFGb	
		dc.w SME_SaFGb_2EE-SME_SaFGb, SME_SaFGb_2F9-SME_SaFGb	
		dc.w SME_SaFGb_304-SME_SaFGb, SME_SaFGb_309-SME_SaFGb	
		dc.w SME_SaFGb_310-SME_SaFGb, SME_SaFGb_313-SME_SaFGb	
		dc.w SME_SaFGb_316-SME_SaFGb, SME_SaFGb_319-SME_SaFGb	
		dc.w SME_SaFGb_320-SME_SaFGb, SME_SaFGb_327-SME_SaFGb	
		dc.w SME_SaFGb_32E-SME_SaFGb, SME_SaFGb_335-SME_SaFGb	
		dc.w SME_SaFGb_33A-SME_SaFGb, SME_SaFGb_343-SME_SaFGb	
		dc.w SME_SaFGb_34C-SME_SaFGb, SME_SaFGb_355-SME_SaFGb	
		dc.w SME_SaFGb_35E-SME_SaFGb, SME_SaFGb_367-SME_SaFGb	
		dc.w SME_SaFGb_370-SME_SaFGb, SME_SaFGb_37F-SME_SaFGb	
		dc.w SME_SaFGb_38E-SME_SaFGb, SME_SaFGb_39D-SME_SaFGb	
		dc.w SME_SaFGb_3AA-SME_SaFGb, SME_SaFGb_3BB-SME_SaFGb	
		dc.w SME_SaFGb_3CA-SME_SaFGb, SME_SaFGb_3DD-SME_SaFGb	
		dc.w SME_SaFGb_3EC-SME_SaFGb, SME_SaFGb_3F7-SME_SaFGb	
		dc.w SME_SaFGb_402-SME_SaFGb, SME_SaFGb_40D-SME_SaFGb	
		dc.w SME_SaFGb_418-SME_SaFGb, SME_SaFGb_429-SME_SaFGb	
		dc.w SME_SaFGb_43C-SME_SaFGb, SME_SaFGb_44F-SME_SaFGb	
SME_SaFGb_DC:	dc.b 0	
SME_SaFGb_DD:	dc.b 4, $20, 0, $70, 3, $20, $B, $20, $E	
SME_SaFGb_E6:	dc.b 3, $50, $11, $50, $17, $20, $1D	
SME_SaFGb_ED:	dc.b 3, $50, $20, $50, $17, $20, $1D	
SME_SaFGb_F4:	dc.b 3, $50, $20, $50, $17, $20, $26	
SME_SaFGb_FB:	dc.b 3, $80, $29, $20, $B, $20, $E	
SME_SaFGb_102:	dc.b 4, $70, $32, $50, $3A, $50, $40, $10, $46	
SME_SaFGb_10B:	dc.b 2, $70, $32, $B0, $48	
SME_SaFGb_110:	dc.b 2, $50, $54, $80, $5A	
SME_SaFGb_115:	dc.b 4, $50, $54, $50, $63, $50, $69, $10, $6F	
SME_SaFGb_11E:	dc.b 2, $50, $54, $B0, $71	
SME_SaFGb_123:	dc.b 3, $70, $32, $30, $7D, $50, $81	
SME_SaFGb_12A:	dc.b 5, $50, $87, $50, $8D, $20, $93, $50, $96, 0, $9C	
SME_SaFGb_135:	dc.b 6, $50, $87, $10, $9D, $30, $9F, $50, $A3, $30, $A9, 0, $AD	
SME_SaFGb_142:	dc.b 4, $50, $AE, $10, $B4, $70, $B6, $20, $BE	
SME_SaFGb_14B:	dc.b 5, $50, $C1, $30, $C7, $70, $CB, $20, $D3, $10, $D6	
SME_SaFGb_156:	dc.b 4, $50, $C1, $10, $D8, $70, $DA, $20, $E2	
SME_SaFGb_15F:	dc.b 5, $50, $87, $10, $9D, 0, $93, $70, $E5, $20, $ED	
SME_SaFGb_16A:	dc.b 4, $70, $F0, $50, $F8, $10, $FE, $51, 0	
SME_SaFGb_173:	dc.b 2, $70, $F0, $B1, 6	
SME_SaFGb_178:	dc.b 2, $51, $12, $81, $18	
SME_SaFGb_17D:	dc.b 4, $51, $12, $51, $21, $11, $27, $51, $29	
SME_SaFGb_186:	dc.b 2, $51, $12, $B1, $2F	
SME_SaFGb_18B:	dc.b 3, $70, $F0, 1, 6, $81, $3B	
SME_SaFGb_192:	dc.b 6, $51, $44, $11, $4A, $11, $4C, $81, $4E, 1, $57, 1, $58	
SME_SaFGb_19F:	dc.b 6, $51, $44, $21, $59, $11, $5C, $11, $5E, $81, $60, 1, $57	
SME_SaFGb_1AC:	dc.b 4, $51, $69, $11, $6F, $81, $71, $11, $7A	
SME_SaFGb_1B5:	dc.b 5, $51, $7C, $21, $82, $11, $85, $71, $87, $21, $8F	
SME_SaFGb_1C0:	dc.b 4, $51, $7C, $11, $92, $81, $94, $11, $9D	
SME_SaFGb_1C9:	dc.b 5, $51, $44, $81, $9F, $11, $5E, $11, $A8, 1, $57	
SME_SaFGb_1D4:	dc.b 2, $51, $AA, $B1, $B0	
SME_SaFGb_1D9:	dc.b 2, $50, $54, $B1, $BC	
SME_SaFGb_1DE:	dc.b 2, $51, $AA, $B1, $C8	
SME_SaFGb_1E3:	dc.b 2, $50, $54, $B1, $D4	
SME_SaFGb_1E8:	dc.b 4, $51, $E0, $11, $E6, $B1, $E8, 1, $F4	
SME_SaFGb_1F1:	dc.b 3, $51, $F5, $11, $FB, $B1, $FD	
SME_SaFGb_1F8:	dc.b 4, $51, $E0, $12, 9, $B2, $B, 1, $F4	
SME_SaFGb_201:	dc.b 3, $51, $F5, $11, $FB, $B2, $17	
SME_SaFGb_208:	dc.b 2, $52, $23, $B2, $29	
SME_SaFGb_20D:	dc.b 2, $51, $12, $B2, $35	
SME_SaFGb_212:	dc.b 2, $52, $23, $B2, $41	
SME_SaFGb_217:	dc.b 2, $51, $12, $B2, $4D	
SME_SaFGb_21C:	dc.b 4, $52, $59, $12, $5F, $B2, $61, 2, $6D	
SME_SaFGb_225:	dc.b 2, $72, $6E, $B2, $76	
SME_SaFGb_22A:	dc.b 4, $52, $59, $12, $82, $B2, $84, 2, $6D	
SME_SaFGb_233:	dc.b 2, $72, $6E, $B2, $90	
SME_SaFGb_238:	dc.b 1, $F2, $9C	
SME_SaFGb_23B:	dc.b 1, $F2, $AC	
SME_SaFGb_23E:	dc.b 1, $F2, $BC	
SME_SaFGb_241:	dc.b 1, $F2, $CC	
SME_SaFGb_244:	dc.b 1, $F2, $DC	
SME_SaFGb_247:	dc.b 2, $B2, $EC, $22, $F8	
SME_SaFGb_24C:	dc.b 1, $F2, $FB	
SME_SaFGb_24F:	dc.b 2, $B3, $B, $23, $17	
SME_SaFGb_254:	dc.b 1, $F3, $1A	
SME_SaFGb_257:	dc.b 2, $53, $2A, $B3, $30	
SME_SaFGb_25C:	dc.b 4, $53, $3C, $73, $42, $13, $4A, 3, $4C	
SME_SaFGb_265:	dc.b 4, $13, $4D, $73, $4F, $23, $57, 3, $5A	
SME_SaFGb_26E:	dc.b 3, $23, $5B, $23, $5E, $F3, $61	
SME_SaFGb_275:	dc.b 3, $B3, $71, $73, $7D, 0, $71	
SME_SaFGb_27C:	dc.b 3, $73, $85, $33, $8D, $23, $91	
SME_SaFGb_283:	dc.b 1, $83, $94	
SME_SaFGb_286:	dc.b 3, $73, $9D, 3, $A5, $33, $A6	
SME_SaFGb_28D:	dc.b 3, $73, $AA, $33, $B2, $23, $B6	
SME_SaFGb_294:	dc.b 3, $B3, $B9, $13, $C5, 3, $C7	
SME_SaFGb_29B:	dc.b 4, $B3, $C8, $33, $D4, 3, $D8, 3, $D9	
SME_SaFGb_2A4:	dc.b 4, $B3, $DA, $33, $E6, 3, $EA, 3, $EB	
SME_SaFGb_2AD:	dc.b 5, $83, $EC, $13, $F5, $53, $F7, $13, $FD, 3, $FF	
SME_SaFGb_2B8:	dc.b 5, $84, 0, $14, 9, $53, $F7, $13, $FD, 3, $FF	
SME_SaFGb_2C3:	dc.b 2, $84, $B, $74, $14	
SME_SaFGb_2C8:	dc.b 3, $84, $1C, $24, $25, $14, $28	
SME_SaFGb_2CF:	dc.b 2, $84, $2A, $74, $33	
SME_SaFGb_2D4:	dc.b 3, $84, $1C, $24, $3B, $14, $3E	
SME_SaFGb_2DB:	dc.b 2, $54, $40, $B4, $46	
SME_SaFGb_2E0:	dc.b 3, $84, $52, $34, $5B, 4, $5F	
SME_SaFGb_2E7:	dc.b 3, $74, $60, $14, $68, $B4, $6A	
SME_SaFGb_2EE:	dc.b 5, $74, $76, $14, $7E, $54, $80, $34, $86, 4, $8A	
SME_SaFGb_2F9:	dc.b 5, $74, $8B, $14, $7E, $54, $93, $34, $86, 4, $8A	
SME_SaFGb_304:	dc.b 2, $24, $99, $F4, $9C	
SME_SaFGb_309:	dc.b 3, $24, $AC, $B4, $AF, $24, $BB	
SME_SaFGb_310:	dc.b 1, $B4, $BE	
SME_SaFGb_313:	dc.b 1, $54, $CA	
SME_SaFGb_316:	dc.b 1, $14, $D0	
SME_SaFGb_319:	dc.b 3, $73, $85, $33, $8D, $23, $91	
SME_SaFGb_320:	dc.b 3, $73, $9D, 3, $A5, $33, $A6	
SME_SaFGb_327:	dc.b 3, $B4, $D2, $14, $DE, $34, $E0	
SME_SaFGb_32E:	dc.b 3, $54, $E4, $B4, $EA, $10, $6D	
SME_SaFGb_335:	dc.b 2, $F4, $F6, $25, 6	
SME_SaFGb_33A:	dc.b 4, $26, $51, $36, $54, $36, $58, $26, $5C	
SME_SaFGb_343:	dc.b 4, $26, $5F, $36, $62, $36, $66, $26, $6A	
SME_SaFGb_34C:	dc.b 4, $26, $6D, $36, $70, $36, $74, $26, $78	
SME_SaFGb_355:	dc.b 4, $26, $7B, $36, $7E, $36, $82, $26, $86	
SME_SaFGb_35E:	dc.b 4, $26, $89, $36, $8C, $36, $90, $26, $94	
SME_SaFGb_367:	dc.b 4, $26, $97, $36, $9A, $36, $9E, $26, $A2	
SME_SaFGb_370:	dc.b 7, $35, 9, 5, $D, $35, $E, 5, $12, $35, $13, 5, $17, $35, $18	
SME_SaFGb_37F:	dc.b 7, $35, $1C, 5, $20, $35, $21, 5, $25, $35, $26, 5, $2A, $35, $2B	
SME_SaFGb_38E:	dc.b 7, $35, $2F, 5, $33, $35, $34, 5, $38, $35, $39, 5, $3D, $35, $3E	
SME_SaFGb_39D:	dc.b 6, $35, $42, $35, $46, 5, $4A, $35, $4B, 5, $4F, $35, $50	
SME_SaFGb_3AA:	dc.b 8, $25, $54, $35, $57, 5, $5B, $35, $5C, 5, $60, $35, $61, 5, $65, $25, $66	
SME_SaFGb_3BB:	dc.b 7, $35, $69, $35, $6D, 5, $71, $35, $72, 5, $76, $35, $77, $35, $7B	
SME_SaFGb_3CA:	dc.b 9, $25, $7F, $35, $82, 5, $86, $35, $87, 5, $8B, $35, $8C, 5, $90, $35, $91, 5, $95	
SME_SaFGb_3DD:	dc.b 7, $35, $96, $35, $9A, 5, $9E, $35, $9F, 5, $A3, $35, $A4, $25, $A8	
SME_SaFGb_3EC:	dc.b 5, $25, $AB, $35, $AE, $35, $B2, $35, $B6, $35, $BA	
SME_SaFGb_3F7:	dc.b 5, $25, $BE, $35, $C1, $35, $C5, $35, $C9, $35, $CD	
SME_SaFGb_402:	dc.b 5, $25, $D1, $35, $D4, $35, $D8, $35, $DC, $35, $E0	
SME_SaFGb_40D:	dc.b 5, $15, $E4, $35, $E6, $35, $EA, $35, $EE, $35, $F2	
SME_SaFGb_418:	dc.b 8, $35, $F6, 5, $FA, $35, $FB, $15, $FF, $36, 1, 6, 5, $36, 6, $26, $A	
SME_SaFGb_429:	dc.b 9, $36, $D, 6, $11, $36, $12, 6, $16, $36, $17, 6, $1B, $36, $1C, 6, $20, $16, $21	
SME_SaFGb_43C:	dc.b 9, $36, $23, 6, $27, $36, $28, $16, $2C, $36, $2E, 6, $32, $36, $33, 6, $37, $26, $38	
SME_SaFGb_44F:	dc.b 9, $36, $3B, 6, $3F, $36, $40, 6, $44, $36, $45, 6, $49, $36, $4A, 6, $4E, $16, $4F	
		even