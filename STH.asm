		tst.w	($FFFFFE08).w	; is debug mode	being used?
		beq.s	Obj01_Normal	; if not, branch
		jmp	DebugMode
; ===========================================================================

Obj01_Normal:
		moveq	#0,d0
		move.b	$24(a0),d0
		move.w	Obj01_Index(pc,d0.w),d1
		jmp	Obj01_Index(pc,d1.w)
; ===========================================================================
Obj01_Index:	dc.w Obj01_Main-Obj01_Index
		dc.w Obj01_Control-Obj01_Index
		dc.w Obj01_Hurt-Obj01_Index
		dc.w Obj01_Death-Obj01_Index
		dc.w Obj01_ResetLevel-Obj01_Index
; ===========================================================================

Obj01_Main:				; XREF: Obj01_Index
		addq.b	#2,$24(a0)
		move.b	#$13,$16(a0)
		move.b	#9,$17(a0)
		move.l	#Map_Sonic,4(a0)
		move.w	#$780,2(a0)
		move.b	#2,$18(a0)
		move.b	#$18,$19(a0)
		move.b	#4,1(a0)
		move.w	#$600,($FFFFF760).w ; Sonic's top speed
		move.w	#$C,($FFFFF762).w ; Sonic's acceleration
		move.w	#$80,($FFFFF764).w ; Sonic's deceleration
		move.b	#5,$FFFFD1C0.w

Obj01_Control:				; XREF: Obj01_Index
		tst.w	($FFFFFFFA).w	; is debug cheat enabled?
		beq.s	loc_12C58	; if not, branch
		btst	#4,($FFFFF605).w ; is button C pressed?
		beq.s	loc_12C58	; if not, branch
		move.w	#1,($FFFFFE08).w ; change Sonic	into a ring/item
		clr.b	($FFFFF7CC).w
		rts	
; ===========================================================================

loc_12C58:
		tst.b	($FFFFF7CC).w	; are controls locked?
		bne.s	loc_12C64	; if yes, branch
		move.w	($FFFFF604).w,($FFFFF602).w ; enable joypad control

loc_12C64:
		btst	#0,($FFFFF7C8).w ; are controls	locked?
		bne.s	loc_12C7E	; if yes, branch
		moveq	#0,d0
		move.b	$22(a0),d0
		andi.w	#6,d0
		move.w	Obj01_Modes(pc,d0.w),d1
		jsr	Obj01_Modes(pc,d1.w)

loc_12C7E:
		bsr.s	Sonic_Display
		bsr.w	Sonic_RecordPos
		bsr.w	Sonic_Water
		move.b	($FFFFF768).w,$36(a0)
		move.b	($FFFFF76A).w,$37(a0)
		tst.b	($FFFFF7C7).w
		beq.s	loc_12CA6
		tst.b	$1C(a0)
		bne.s	loc_12CA6
		move.b	$1D(a0),$1C(a0)

loc_12CA6:
		bsr.w	Sonic_Animate
		tst.b	($FFFFF7C8).w
		bmi.s	loc_12CB6
		jsr	TouchResponse

loc_12CB6:
		bsr.w	Sonic_Loops
		bsr.w	LoadSonicDynPLC
		rts	
; ===========================================================================
Obj01_Modes:	dc.w Obj01_MdNormal-Obj01_Modes
		dc.w Obj01_MdJump-Obj01_Modes
		dc.w Obj01_MdRoll-Obj01_Modes
		dc.w Obj01_MdJump2-Obj01_Modes
; ---------------------------------------------------------------------------
; Music	to play	after invincibility wears off
; ---------------------------------------------------------------------------
MusicList2:	incbin	misc\muslist2.bin
		even
; ===========================================================================

Sonic_Display:				; XREF: loc_12C7E
		move.w	$30(a0),d0
		beq.s	Obj01_Display
		subq.w	#1,$30(a0)
		lsr.w	#3,d0
		bcc.s	Obj01_ChkInvin

Obj01_Display:
		jsr	DisplaySprite

Obj01_ChkInvin:
		tst.b	($FFFFFE2D).w	; does Sonic have invincibility?
		beq.s	Obj01_ChkShoes	; if not, branch
		tst.w	$32(a0)		; check	time remaining for invinciblity
		beq.s	Obj01_ChkShoes	; if no	time remains, branch
		subq.w	#1,$32(a0)	; subtract 1 from time
		bne.s	Obj01_ChkShoes
		tst.b	($FFFFF7AA).w
		bne.s	Obj01_RmvInvin
		cmpi.w	#$C,($FFFFFE14).w
		bcs.s	Obj01_RmvInvin
		moveq	#0,d0
		move.b	($FFFFFE10).w,d0
		cmpi.w	#$103,($FFFFFE10).w ; check if level is	SBZ3
		bne.s	Obj01_PlayMusic
		moveq	#5,d0		; play SBZ music

Obj01_PlayMusic:
		lea	(MusicList2).l,a1
		move.b	(a1,d0.w),d0
		jsr	(PlaySound).l	; play normal music

Obj01_RmvInvin:
		move.b	#0,($FFFFFE2D).w ; cancel invincibility

Obj01_ChkShoes:
		tst.b	($FFFFFE2E).w	; does Sonic have speed	shoes?
		beq.s	Obj01_ExitChk	; if not, branch
		tst.w	$34(a0)		; check	time remaining
		beq.s	Obj01_ExitChk
		subq.w	#1,$34(a0)	; subtract 1 from time
		bne.s	Obj01_ExitChk
		move.w	#$600,($FFFFF760).w ; restore Sonic's speed
		move.w	#$C,($FFFFF762).w ; restore Sonic's acceleration
		move.w	#$80,($FFFFF764).w ; restore Sonic's deceleration
		move.b	#0,($FFFFFE2E).w ; cancel speed	shoes
		move.w	#$E3,d0
		jmp	(PlaySound).l	; run music at normal speed
; ===========================================================================

Obj01_ExitChk:
		rts	

; ---------------------------------------------------------------------------
; Subroutine to	record Sonic's previous positions for invincibility stars
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_RecordPos:			; XREF: loc_12C7E; Obj01_Hurt; Obj01_Death
		move.w	($FFFFF7A8).w,d0
		lea	($FFFFCB00).w,a1
		lea	(a1,d0.w),a1
		move.w	8(a0),(a1)+
		move.w	$C(a0),(a1)+
		addq.b	#4,($FFFFF7A9).w
		rts	
; End of function Sonic_RecordPos

; ---------------------------------------------------------------------------
; Subroutine for Sonic when he's underwater
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Water:				; XREF: loc_12C7E
		cmpi.b	#1,($FFFFFE10).w ; is level LZ?
		beq.s	Obj01_InWater	; if yes, branch

locret_12D80:
		rts	
; ===========================================================================

Obj01_InWater:
		move.w	($FFFFF646).w,d0
		cmp.w	$C(a0),d0	; is Sonic above the water?
		bge.s	Obj01_OutWater	; if yes, branch
		bset	#6,$22(a0)
		bne.s	locret_12D80
		bsr.w	ResumeMusic
		move.b	#$A,($FFFFD340).w ; load bubbles object	from Sonic's mouth
		move.b	#$81,($FFFFD368).w
		move.w	#$300,($FFFFF760).w ; change Sonic's top speed
		move.w	#6,($FFFFF762).w ; change Sonic's acceleration
		move.w	#$40,($FFFFF764).w ; change Sonic's deceleration
		asr	$10(a0)
		asr	$12(a0)
		asr	$12(a0)
		beq.s	locret_12D80
		move.b	#8,($FFFFD300).w ; load	splash object
		move.w	#$AA,d0
		jmp	(PlaySound_Special).l ;	play splash sound
; ===========================================================================

Obj01_OutWater:
		bclr	#6,$22(a0)
		beq.s	locret_12D80
		bsr.w	ResumeMusic
		move.w	#$600,($FFFFF760).w ; restore Sonic's speed
		move.w	#$C,($FFFFF762).w ; restore Sonic's acceleration
		move.w	#$80,($FFFFF764).w ; restore Sonic's deceleration
		asl	$12(a0)
		beq.w	locret_12D80
		move.b	#8,($FFFFD300).w ; load	splash object
		cmpi.w	#-$1000,$12(a0)
		bgt.s	loc_12E0E
		move.w	#-$1000,$12(a0)	; set maximum speed on leaving water

loc_12E0E:
		move.w	#$AA,d0
		jmp	(PlaySound_Special).l ;	play splash sound
; End of function Sonic_Water

; ===========================================================================
; ---------------------------------------------------------------------------
; Modes	for controlling	Sonic
; ---------------------------------------------------------------------------

Obj01_MdNormal:				; XREF: Obj01_Modes
		bsr.w	Sonic_Peelout
		bsr.w	Sonic_Spindash	; add this line!
		bsr.w	Sonic_Jump
		bsr.w	Sonic_SlopeResist
		bsr.w	Sonic_Move
		bsr.w	Sonic_Roll
		bsr.w	Sonic_LevelBound
		jsr	SpeedToPos
		bsr.w	Sonic_AnglePos
		bsr.w	Sonic_SlopeRepel
		rts	
; ===========================================================================

Obj01_MdJump:				; XREF: Obj01_Modes
		clr.b	$39(a0)
		bsr.w	Sonic_JumpHeight
		bsr.w	Sonic_ChgJumpDir
		bsr.w	Sonic_LevelBound
		jsr	ObjectFall
		btst	#6,$22(a0)
		beq.s	loc_12E5C
		subi.w	#$28,$12(a0)

loc_12E5C:
		bsr.w	Sonic_JumpAngle
		bsr.w	Sonic_Floor
		rts	
; ===========================================================================

Obj01_MdRoll:				; XREF: Obj01_Modes
		bsr.w	Sonic_Jump
		bsr.w	Sonic_RollRepel
		bsr.w	Sonic_RollSpeed
		bsr.w	Sonic_LevelBound
		jsr	SpeedToPos
		bsr.w	Sonic_AnglePos
		bsr.w	Sonic_SlopeRepel
		rts	
; ===========================================================================

Obj01_MdJump2:				; XREF: Obj01_Modes
		clr.b	$39(a0)
		bsr.w	Sonic_JumpHeight
		bsr.w	Sonic_ChgJumpDir
		bsr.w	Sonic_LevelBound
		jsr	ObjectFall
		btst	#6,$22(a0)
		beq.s	loc_12EA6
		subi.w	#$28,$12(a0)
		move.w	$10(a0),d0	; move Sonic's X-velocity to d0 
		tst.w	d0	; is his speed positive? (is he running to the right?)
		bpl.s	Obj01_MdJump2_Abs	; if yes, branch
		neg.w	d0	; otherwise negate it

Obj01_MdJump2_Abs:
		cmpi.w	#$250,d0	; if Sonic speed less than $250?
		blt.s	loc_12EA6	; if yes, branch
		move.w	$C(a0),d0	; move sonic's Y-position to d0
		sub.w	($FFFFF646).w,d0	; sub the water height from it
		cmpi.w	#$F,d0	; is Sonic slightly in the water?
		bgt.s	loc_12EA6	; if not, branch
		subi.w	#$90,$12(a0)	; jump out of water

loc_12EA6:
		bsr.w	Sonic_JumpAngle
		bsr.w	Sonic_Floor
		rts	
; ---------------------------------------------------------------------------
; Subroutine to	make Sonic walk/run
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Move:				; XREF: Obj01_MdNormal
		move.w	($FFFFF760).w,d6
		move.w	($FFFFF762).w,d5
		move.w	($FFFFF764).w,d4
		tst.b	($FFFFF7CA).w
		bne.w	loc_12FEE
		tst.w	$3E(a0)
		bne.w	Obj01_ResetScr
		btst	#2,($FFFFF602).w ; is left being pressed?
		beq.s	Obj01_NotLeft	; if not, branch
		bsr.w	Sonic_MoveLeft

Obj01_NotLeft:
		btst	#3,($FFFFF602).w ; is right being pressed?
		beq.s	Obj01_NotRight	; if not, branch
		bsr.w	Sonic_MoveRight

Obj01_NotRight:
		move.b	$26(a0),d0
		addi.b	#$20,d0
		andi.b	#$C0,d0		; is Sonic on a	slope?
		bne.w	Obj01_ResetScr	; if yes, branch
		tst.w	$14(a0)		; is Sonic moving?
		bne.w	Obj01_ResetScr	; if yes, branch
		bclr	#5,$22(a0)
		move.b	#5,$1C(a0)	; use "standing" animation
		btst	#3,$22(a0)
		beq.s	Sonic_Balance
		moveq	#0,d0
		move.b	$3D(a0),d0
		lsl.w	#6,d0
		lea	($FFFFD000).w,a1
		lea	(a1,d0.w),a1
		tst.b	$22(a1)
		bmi.s	Sonic_LookUp
		moveq	#0,d1
		move.b	$19(a1),d1
		move.w	d1,d2
		add.w	d2,d2
		subq.w	#4,d2
		add.w	8(a0),d1
		sub.w	8(a1),d1
		cmpi.w	#4,d1
		blt.s	loc_12F6A
		cmp.w	d2,d1
		bge.s	loc_12F5A
		bra.s	Sonic_LookUp
; ===========================================================================

Sonic_Balance:
		jsr	ObjHitFloor
		cmpi.w	#$C,d1
		blt.s	Sonic_LookUp
		cmpi.b	#3,$36(a0)
		bne.s	loc_12F62

loc_12F5A:
		bclr	#0,$22(a0)
		bra.s	loc_12F70
; ===========================================================================

loc_12F62:
		cmpi.b	#3,$37(a0)
		bne.s	Sonic_LookUp

loc_12F6A:
		bset	#0,$22(a0)

loc_12F70:
		move.b	#6,$1C(a0)	; use "balancing" animation
		bra.s	Obj01_ResetScr
; ===========================================================================

Sonic_LookUp:
		btst	#0,($FFFFF602).w ; is up being pressed?
		beq.s	Sonic_Duck	; if not, branch
		move.b	#7,$1C(a0)	; use "looking up" animation
		addq.b	#1,($FFFFC903).w
		cmp.b	#$78,($FFFFC903).w
		bcs.s	Obj01_ResetScr_Part2
		move.b	#$78,($FFFFC903).w
		cmpi.w	#$C8,($FFFFF73E).w
		beq.s	loc_12FC2
		addq.w	#2,($FFFFF73E).w
		bra.s	loc_12FC2
; ===========================================================================

 
Sonic_Duck:
		btst	#1,($FFFFF602).w ; is down being pressed?
		beq.s	Obj01_ResetScr	; if not, branch
		move.b	#8,$1C(a0)	; use "ducking"	animation
		addq.b	#1,($FFFFC903).w
		cmpi.b	#$78,($FFFFC903).w
		bcs.s	Obj01_ResetScr_Part2
		move.b	#$78,($FFFFC903).w
		cmpi.w	#8,($FFFFF73E).w
		beq.s	loc_12FC2
		subq.w	#2,($FFFFF73E).w
		bra.s	loc_12FC2
; ===========================================================================

 
Obj01_ResetScr:
		move.b	#0,($FFFFC903).w
		
Obj01_ResetScr_Part2:
		cmpi.w	#$60,($FFFFF73E).w ; is	screen in its default position?
		beq.s	loc_12FC2	; if yes, branch
		bcc.s	loc_12FBE
		addq.w	#4,($FFFFF73E).w ; move	screen back to default

 
loc_12FBE:
		subq.w	#2,($FFFFF73E).w ; move	screen back to default

loc_12FC2:
		move.b	($FFFFF602).w,d0
		andi.b	#$C,d0		; is left/right	pressed?
		bne.s	loc_12FEE	; if yes, branch
		move.w	$14(a0),d0
		beq.s	loc_12FEE
		bmi.s	loc_12FE2
		sub.w	d5,d0
		bcc.s	loc_12FDC
		move.w	#0,d0

loc_12FDC:
		move.w	d0,$14(a0)
		bra.s	loc_12FEE
; ===========================================================================

loc_12FE2:
		add.w	d5,d0
		bcc.s	loc_12FEA
		move.w	#0,d0

loc_12FEA:
		move.w	d0,$14(a0)

loc_12FEE:
		move.b	$26(a0),d0
		jsr	(CalcSine).l
		muls.w	$14(a0),d1
		asr.l	#8,d1
		move.w	d1,$10(a0)
		muls.w	$14(a0),d0
		asr.l	#8,d0
		move.w	d0,$12(a0)

loc_1300C:
		move.b	$26(a0),d0
		addi.b	#$40,d0
		bmi.s	locret_1307C
		move.b	#$40,d1
		tst.w	$14(a0)
		beq.s	locret_1307C
		bmi.s	loc_13024
		neg.w	d1

loc_13024:
		move.b	$26(a0),d0
		add.b	d1,d0
		move.w	d0,-(sp)
		bsr.w	Sonic_WalkSpeed
		move.w	(sp)+,d0
		tst.w	d1
		bpl.s	locret_1307C
		asl.w	#8,d1
		addi.b	#$20,d0
		andi.b	#$C0,d0
		beq.s	loc_13078
		cmpi.b	#$40,d0
		beq.s	loc_13066
		cmpi.b	#$80,d0
		beq.s	loc_13060
		add.w	d1,$10(a0)
		bset	#5,$22(a0)
		move.w	#0,$14(a0)
		rts	
; ===========================================================================

loc_13060:
		sub.w	d1,$12(a0)
		rts	
; ===========================================================================

loc_13066:
		sub.w	d1,$10(a0)
		bset	#5,$22(a0)
		move.w	#0,$14(a0)
		rts	
; ===========================================================================

loc_13078:
		add.w	d1,$12(a0)

locret_1307C:
		rts	
; End of function Sonic_Move


; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_MoveLeft:		   ; XREF: Sonic_Move
		move.w	$14(a0),d0
		beq.s	loc_13086
		bpl.s	loc_130B2

loc_13086:
		bset	#0,$22(a0)
		bne.s	loc_1309A
		bclr	#5,$22(a0)
		move.b	#1,$1D(a0)

loc_1309A:
		sub.w	d5,d0
		move.w	d6,d1
		neg.w	d1
		cmp.w	d1,d0
		bgt.s	loc_130A6
		add.w	d5,d0
		cmp.w	d1,d0
		ble.s	loc_130A6
		move.w	d1,d0

loc_130A6:
		move.w	d0,$14(a0)
		move.b	#0,$1C(a0); use walking animation
		rts
; ===========================================================================

loc_130B2:				; XREF: Sonic_MoveLeft
		sub.w	d4,d0
		bcc.s	loc_130BA
		move.w	#-$80,d0

loc_130BA:
		move.w	d0,$14(a0)
		move.b	$26(a0),d0
		addi.b	#$20,d0
		andi.b	#$C0,d0
		bne.s	locret_130E8
		cmpi.w	#$400,d0
		blt.s	locret_130E8
		move.b	#$D,$1C(a0)	; use "stopping" animation
		bclr	#0,$22(a0)
		move.w	#$A4,d0
		jsr	(PlaySound_Special).l ;	play stopping sound

locret_130E8:
		rts	
; End of function Sonic_MoveLeft


; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_MoveRight:	   ; XREF: Sonic_Move
		move.w	$14(a0),d0
		bmi.s	loc_13118
		bclr	#0,$22(a0)
		beq.s	loc_13104
		bclr	#5,$22(a0)
		move.b	#1,$1D(a0)

loc_13104:
		add.w	d5,d0
		cmp.w	d6,d0
		blt.s	loc_1310C
		sub.w	d5,d0
		cmp.w	d6,d0
		bge.s	loc_1310C
		move.w	d6,d0

loc_1310C:
		move.w	d0,$14(a0)
		move.b	#0,$1C(a0); use walking animation
		rts
; ===========================================================================

loc_13118:				; XREF: Sonic_MoveRight
		add.w	d4,d0
		bcc.s	loc_13120
		move.w	#$80,d0

loc_13120:
		move.w	d0,$14(a0)
		move.b	$26(a0),d0
		addi.b	#$20,d0
		andi.b	#$C0,d0
		bne.s	locret_1314E
		cmpi.w	#-$400,d0
		bgt.s	locret_1314E
		move.b	#$D,$1C(a0)	; use "stopping" animation
		bset	#0,$22(a0)
		move.w	#$A4,d0
		jsr	(PlaySound_Special).l ;	play stopping sound

locret_1314E:
		rts	
; End of function Sonic_MoveRight

; ---------------------------------------------------------------------------
; Subroutine to	change Sonic's speed as he rolls
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_RollSpeed:			; XREF: Obj01_MdRoll
		move.w	($FFFFF760).w,d6
		asl.w	#1,d6
		move.w	($FFFFF762).w,d5
		asr.w	#1,d5
		move.w	($FFFFF764).w,d4
		asr.w	#2,d4
		tst.b	($FFFFF7CA).w
		bne.w	loc_131CC
		tst.w	$3E(a0)
		bne.s	loc_13188
		btst	#2,($FFFFF602).w ; is left being pressed?
		beq.s	loc_1317C	; if not, branch
		bsr.w	Sonic_RollLeft

loc_1317C:
		btst	#3,($FFFFF602).w ; is right being pressed?
		beq.s	loc_13188	; if not, branch
		bsr.w	Sonic_RollRight

loc_13188:
		move.w	$14(a0),d0
		beq.s	loc_131AA
		bmi.s	loc_1319E
		sub.w	d5,d0
		bcc.s	loc_13198
		move.w	#0,d0

loc_13198:
		move.w	d0,$14(a0)
		bra.s	loc_131AA
; ===========================================================================

loc_1319E:				; XREF: Sonic_RollSpeed
		add.w	d5,d0
		bcc.s	loc_131A6
		move.w	#0,d0

loc_131A6:
		move.w	d0,$14(a0)

loc_131AA:
		tst.w	$14(a0)		; is Sonic moving?
		bne.s	loc_131CC	; if yes, branch
		bclr	#2,$22(a0)
		move.b	#$13,$16(a0)
		move.b	#9,$17(a0)
		move.b	#5,$1C(a0)	; use "standing" animation
		subq.w	#5,$C(a0)

loc_131CC:
		cmp.w	#$60,($FFFFF73E).w
		beq.s	@cont2
		bcc.s	@cont1
		addq.w	#4,($FFFFF73E).w
		
@cont1:
		subq.w	#2,($FFFFF73E).w
		
@cont2:
		move.b	$26(a0),d0
		jsr	(CalcSine).l
		muls.w	$14(a0),d0
		asr.l	#8,d0
		move.w	d0,$12(a0)
		muls.w	$14(a0),d1
		asr.l	#8,d1
		cmpi.w	#$1000,d1
		ble.s	loc_131F0
		move.w	#$1000,d1

loc_131F0:
		cmpi.w	#-$1000,d1
		bge.s	loc_131FA
		move.w	#-$1000,d1

loc_131FA:
		move.w	d1,$10(a0)
		bra.w	loc_1300C
; End of function Sonic_RollSpeed


; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_RollLeft:				; XREF: Sonic_RollSpeed
		move.w	$14(a0),d0
		beq.s	loc_1320A
		bpl.s	loc_13218

loc_1320A:
		bset	#0,$22(a0)
		move.b	#2,$1C(a0)	; use "rolling"	animation
		rts	
; ===========================================================================

loc_13218:
		sub.w	d4,d0
		bcc.s	loc_13220
		move.w	#-$80,d0

loc_13220:
		move.w	d0,$14(a0)
		rts	
; End of function Sonic_RollLeft


; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_RollRight:			; XREF: Sonic_RollSpeed
		move.w	$14(a0),d0
		bmi.s	loc_1323A
		bclr	#0,$22(a0)
		move.b	#2,$1C(a0)	; use "rolling"	animation
		rts	
; ===========================================================================

loc_1323A:
		add.w	d4,d0
		bcc.s	loc_13242
		move.w	#$80,d0

loc_13242:
		move.w	d0,$14(a0)
		rts	
; End of function Sonic_RollRight

; ---------------------------------------------------------------------------
; Subroutine to	change Sonic's direction while jumping
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_ChgJumpDir:		; XREF: Obj01_MdJump; Obj01_MdJump2
		move.w	($FFFFF760).w,d6
		move.w	($FFFFF762).w,d5
		asl.w	#1,d5
		btst	#4,$22(a0)	
		bne.s	Obj01_ResetScr2	
		move.w	$10(a0),d0	
		btst	#2,($FFFFF602).w; is left being pressed?	
		beq.s	loc_13278; if not, branch	
		bset	#0,$22(a0)	
		sub.w	d5,d0	
		move.w	d6,d1	
		neg.w	d1	
		cmp.w	d1,d0	
		bgt.s	loc_13278	
		add.w	d5,d0		; +++ remove this frame's acceleration change
		cmp.w	d1,d0		; +++ compare speed with top speed
		ble.s	loc_13278	; +++ if speed was already greater than the maximum, branch	
		move.w	d1,d0

loc_13278:
		btst	#3,($FFFFF602).w; is right being pressed?	
		beq.s	Obj01_JumpMove; if not, branch	
		bclr	#0,$22(a0)	
		add.w	d5,d0	
		cmp.w	d6,d0	
		blt.s	Obj01_JumpMove
		sub.w	d5,d0		; +++ remove this frame's acceleration change
		cmp.w	d6,d0		; +++ compare speed with top speed
		bge.s	Obj01_JumpMove	; +++ if speed was already greater than the maximum, branch
		move.w	d6,d0

Obj01_JumpMove:
		move.w	d0,$10(a0)	; change Sonic's horizontal speed

Obj01_ResetScr2:
		cmpi.w	#$60,($FFFFF73E).w ; is	the screen in its default position?
		beq.s	loc_132A4	; if yes, branch
		bcc.s	loc_132A0
		addq.w	#4,($FFFFF73E).w

loc_132A0:
		subq.w	#2,($FFFFF73E).w

loc_132A4:
		cmpi.w	#-$400,$12(a0)	; is Sonic moving faster than -$400 upwards?
		bcs.s	locret_132D2	; if yes, branch
		move.w	$10(a0),d0
		move.w	d0,d1
		asr.w	#5,d1
		beq.s	locret_132D2
		bmi.s	loc_132C6
		sub.w	d1,d0
		bcc.s	loc_132C0
		sub.w d5,d0
		cmp.w d6,d0
		bge.s Obj01_JumpMove
		move.w	#0,d0

loc_132C0:
		move.w	d0,$10(a0)
		rts	
; ===========================================================================

loc_132C6:
		sub.w	d1,d0
		bcs.s	loc_132CE
		move.w	#0,d0

loc_132CE:
		move.w	d0,$10(a0)

locret_132D2:
		rts	
; End of function Sonic_ChgJumpDir

; ===========================================================================
; ---------------------------------------------------------------------------
; Unused subroutine to squash Sonic
; ---------------------------------------------------------------------------
		move.b	$26(a0),d0
		addi.b	#$20,d0
		andi.b	#$C0,d0
		bne.s	locret_13302
		bsr.w	Sonic_DontRunOnWalls
		tst.w	d1
		bpl.s	locret_13302
		move.w	#0,$14(a0)	; stop Sonic moving
		move.w	#0,$10(a0)
		move.w	#0,$12(a0)
		move.b	#$B,$1C(a0)	; use "warping"	animation

locret_13302:
		rts	
; ---------------------------------------------------------------------------
; Subroutine to	prevent	Sonic leaving the boundaries of	a level
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_LevelBound:			; XREF: Obj01_MdNormal; et al
		move.l	8(a0),d1
		move.w	$10(a0),d0
		ext.l	d0
		asl.l	#8,d0
		add.l	d0,d1
		swap	d1
		move.w	($FFFFF728).w,d0
		addi.w	#$10,d0
		cmp.w	d1,d0		; has Sonic touched the	side boundary?
		bhi.s	Boundary_Sides	; if yes, branch
		move.w	($FFFFF72A).w,d0
		addi.w	#$128,d0
		tst.b	($FFFFF7AA).w
		bne.s	loc_13332
		addi.w	#$40,d0

loc_13332:
		cmp.w	d1,d0		; has Sonic touched the	side boundary?
		bls.s	Boundary_Sides	; if yes, branch

loc_13336:
		move.w	($FFFFF72E).w,d0
		addi.w	#$E0,d0
		cmp.w	$C(a0),d0	; has Sonic touched the	bottom boundary?
		blt.s	Boundary_Bottom	; if yes, branch
		rts	
; ===========================================================================

Boundary_Bottom:
		move.w	($FFFFF726).w,d0
		move.w	($FFFFF72E).w,d1
		cmp.w	d0,d1			; screen still scrolling down?
		blt.s	Boundary_Bottom_locret	; if so, don't kill Sonic
		cmpi.w	#$501,($FFFFFE10).w	; is level SBZ2 ?
		jmp		KillSonic		; if not, kill Sonic
		cmpi.w	#$2000,($FFFFD008).w
		jmp		KillSonic
		clr.b	($FFFFFE30).w		; clear lamppost counter
		move.w	#1,($FFFFFE02).w	; restart the level
		move.w	#$103,($FFFFFE10).w	; set level to SBZ3 (LZ4)
 
Boundary_Bottom_locret:
		rts	
; ===========================================================================

Boundary_Sides:
		move.w	d0,8(a0)
		move.w	#0,$A(a0)
		move.w	#0,$10(a0)	; stop Sonic moving
		move.w	#0,$14(a0)
		bra.s	loc_13336
; End of function Sonic_LevelBound

; ---------------------------------------------------------------------------
; Subroutine allowing Sonic to roll when he's moving
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Roll:				; XREF: Obj01_MdNormal
		tst.b	($FFFFF7CA).w
		bne.s	Obj01_NoRoll
		move.w	$14(a0),d0
		bpl.s	loc_13392
		neg.w	d0

loc_13392:
		cmpi.w	#$80,d0		; is Sonic moving at $80 speed or faster?
		bcs.s	Obj01_NoRoll	; if not, branch
		move.b	($FFFFF602).w,d0
		andi.b	#$C,d0		; is left/right	being pressed?
		bne.s	Obj01_NoRoll	; if yes, branch
		btst	#1,($FFFFF602).w ; is down being pressed?
		bne.s	Obj01_ChkRoll	; if yes, branch

Obj01_NoRoll:
		rts	
; ===========================================================================

Obj01_ChkRoll:
		btst	#2,$22(a0)	; is Sonic already rolling?
		beq.s	Obj01_DoRoll	; if not, branch
		rts	
; ===========================================================================

Obj01_DoRoll:
		bset	#2,$22(a0)
		move.b	#$E,$16(a0)
		move.b	#7,$17(a0)
		move.b	#2,$1C(a0)	; use "rolling"	animation
		addq.w	#5,$C(a0)
		move.w	#$BE,d0
		jsr	(PlaySound_Special).l ;	play rolling sound
		tst.w	$14(a0)
		bne.s	locret_133E8
		move.w	#$200,$14(a0)

locret_133E8:
		rts	
; End of function Sonic_Roll

; ---------------------------------------------------------------------------
; Subroutine allowing Sonic to jump
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Jump:				; XREF: Obj01_MdNormal; Obj01_MdRoll
		move.b	($FFFFF603).w,d0
		andi.b	#$70,d0		; is A,	B or C pressed?
		beq.w	locret_1348E	; if not, branch
		moveq	#0,d0
		move.b	$26(a0),d0
		addi.b	#$80,d0
		bsr.w	sub_14D48
		cmpi.w	#6,d1
		blt.w	locret_1348E
		move.w	#$680,d2
		btst	#6,$22(a0)
		beq.s	loc_1341C
		move.w	#$380,d2

loc_1341C:
		moveq	#0,d0
		move.b	$26(a0),d0
		subi.b	#$40,d0
		jsr	(CalcSine).l
		muls.w	d2,d1
		asr.l	#8,d1
		add.w	d1,$10(a0)	; make Sonic jump
		muls.w	d2,d0
		asr.l	#8,d0
		add.w	d0,$12(a0)	; make Sonic jump
		bset	#1,$22(a0)
		bclr	#5,$22(a0)
		addq.l	#4,sp
		move.b	#1,$3C(a0)
		clr.b	$38(a0)
		move.w	#$A0,d0
		jsr	(PlaySound_Special).l ;	play jumping sound
		move.b	#$13,$16(a0)
		move.b	#9,$17(a0)
		btst	#2,$22(a0)
		bne.s	loc_13490
		move.b	#$E,$16(a0)
		move.b	#7,$17(a0)
		move.b	#2,$1C(a0)	; use "jumping"	animation
		bset	#2,$22(a0)
		addq.w	#5,$C(a0)

locret_1348E:
		rts	
; ===========================================================================

loc_13490:
		bset	#4,$22(a0)
		rts	
; End of function Sonic_Jump


; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_JumpHeight:			; XREF: Obj01_MdJump; Obj01_MdJump2
		tst.b	$3C(a0)
		beq.s	loc_134C4
		move.w	#-$400,d1
		btst	#6,$22(a0)
		beq.s	loc_134AE
		move.w	#-$200,d1

loc_134AE:
		cmp.w	$12(a0),d1
		ble.s	locret_134C2
		move.b	($FFFFF602).w,d0
		andi.b	#$70,d0		; is A,	B or C pressed?
		bne.s	locret_134C2	; if yes, branch
		move.w	d1,$12(a0)

locret_134C2:
		rts	
; ===========================================================================

loc_134C4:
		cmpi.w	#-$FC0,$12(a0)
		bge.s	locret_134D2
		move.w	#-$FC0,$12(a0)

locret_134D2:
		rts	
; End of function Sonic_JumpHeight

; ---------------------------------------------------------------------------
; Subroutine to make Sonic perform a peelout
; ---------------------------------------------------------------------------
 
; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||

Sonic_Peelout:
	include "_new\peelout.asm"
	rts

; ---------------------------------------------------------------------------
; Subroutine to make Sonic perform a Spin Dash
; ---------------------------------------------------------------------------
 
; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||

Sonic_Spindash:
	include	"_new\spindash.asm"
	rts
; ---------------------------------------------------------------------------
; Subroutine to	slow Sonic walking up a	slope
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_SlopeResist:			; XREF: Obj01_MdNormal
		move.b	$26(a0),d0
		addi.b	#$60,d0
		cmpi.b	#$C0,d0
		bcc.s	locret_13508
		move.b	$26(a0),d0
		jsr	(CalcSine).l
		muls.w	#$20,d0
		asr.l	#8,d0
		tst.w	$14(a0)
		beq.s	locret_13508
		bmi.s	loc_13504
		tst.w	d0
		beq.s	locret_13502
		add.w	d0,$14(a0)	; change Sonic's inertia

locret_13502:
		rts	
; ===========================================================================

loc_13504:
		add.w	d0,$14(a0)

locret_13508:
		rts	
; End of function Sonic_SlopeResist

; ---------------------------------------------------------------------------
; Subroutine to	push Sonic down	a slope	while he's rolling
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_RollRepel:			; XREF: Obj01_MdRoll
		move.b	$26(a0),d0
		addi.b	#$60,d0
		cmpi.b	#-$40,d0
		bcc.s	locret_13544
		move.b	$26(a0),d0
		jsr	(CalcSine).l
		muls.w	#$50,d0
		asr.l	#8,d0
		tst.w	$14(a0)
		bmi.s	loc_1353A
		tst.w	d0
		bpl.s	loc_13534
		asr.l	#2,d0

loc_13534:
		add.w	d0,$14(a0)
		rts	
; ===========================================================================

loc_1353A:
		tst.w	d0
		bmi.s	loc_13540
		asr.l	#2,d0

loc_13540:
		add.w	d0,$14(a0)

locret_13544:
		rts	
; End of function Sonic_RollRepel

; ---------------------------------------------------------------------------
; Subroutine to	push Sonic down	a slope
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_SlopeRepel:			; XREF: Obj01_MdNormal; Obj01_MdRoll
		nop	
		tst.b	$38(a0)
		bne.s	locret_13580
		tst.w	$3E(a0)
		bne.s	loc_13582
		move.b	$26(a0),d0
		addi.b	#$20,d0
		andi.b	#$C0,d0
		beq.s	locret_13580
		move.w	$14(a0),d0
		bpl.s	loc_1356A
		neg.w	d0

loc_1356A:
		cmpi.w	#$280,d0
		bcc.s	locret_13580
		clr.w	$14(a0)
		bset	#1,$22(a0)
		move.w	#$1E,$3E(a0)

locret_13580:
		rts	
; ===========================================================================

loc_13582:
		subq.w	#1,$3E(a0)
		rts	
; End of function Sonic_SlopeRepel

; ---------------------------------------------------------------------------
; Subroutine to	return Sonic's angle to 0 as he jumps
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_JumpAngle:			; XREF: Obj01_MdJump; Obj01_MdJump2
		move.b	$26(a0),d0	; get Sonic's angle
		beq.s	locret_135A2	; if already 0,	branch
		bpl.s	loc_13598	; if higher than 0, branch

		addq.b	#2,d0		; increase angle
		bcc.s	loc_13596
		moveq	#0,d0

loc_13596:
		bra.s	loc_1359E
; ===========================================================================

loc_13598:
		subq.b	#2,d0		; decrease angle
		bcc.s	loc_1359E
		moveq	#0,d0

loc_1359E:
		move.b	d0,$26(a0)

locret_135A2:
		rts	
; End of function Sonic_JumpAngle

; ---------------------------------------------------------------------------
; Subroutine for Sonic to interact with	the floor after	jumping/falling
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Floor:				; XREF: Obj01_MdJump; Obj01_MdJump2
		move.w	$10(a0),d1
		move.w	$12(a0),d2
		jsr	(CalcAngle).l
		move.b	d0,($FFFFFFEC).w
		subi.b	#$20,d0
		move.b	d0,($FFFFFFED).w
		andi.b	#$C0,d0
		move.b	d0,($FFFFFFEE).w
		cmpi.b	#$40,d0
		beq.w	loc_13680
		cmpi.b	#$80,d0
		beq.w	loc_136E2
		cmpi.b	#-$40,d0
		beq.w	loc_1373E
		bsr.w	Sonic_HitWall
		tst.w	d1
		bpl.s	loc_135F0
		sub.w	d1,8(a0)
		move.w	#0,$10(a0)

loc_135F0:
		bsr.w	sub_14EB4
		tst.w	d1
		bpl.s	loc_13602
		add.w	d1,8(a0)
		move.w	#0,$10(a0)

loc_13602:
		bsr.w	Sonic_HitFloor
		move.b	d1,($FFFFFFEF).w
		tst.w	d1
		bpl.s	locret_1367E
		move.b	$12(a0),d2
		addq.b	#8,d2
		neg.b	d2
		cmp.b	d2,d1
		bge.s	loc_1361E
		cmp.b	d2,d0
		blt.s	locret_1367E

loc_1361E:
		add.w	d1,$C(a0)
		move.b	d3,$26(a0)
		bsr.w	Sonic_ResetOnFloor
		move.b	#0,$1C(a0)
		move.b	d3,d0
		addi.b	#$20,d0
		andi.b	#$40,d0
		bne.s	loc_1365C
		move.b	d3,d0
		addi.b	#$10,d0
		andi.b	#$20,d0
		beq.s	loc_1364E
		asr	$12(a0)
		bra.s	loc_13670
; ===========================================================================

loc_1364E:
		move.w	#0,$12(a0)
		move.w	$10(a0),$14(a0)
		rts	
; ===========================================================================

loc_1365C:
		move.w	#0,$10(a0)
		cmpi.w	#$FC0,$12(a0)
		ble.s	loc_13670
		move.w	#$FC0,$12(a0)

loc_13670:
		move.w	$12(a0),$14(a0)
		tst.b	d3
		bpl.s	locret_1367E
		neg.w	$14(a0)

locret_1367E:
		rts	
; ===========================================================================

loc_13680:
		bsr.w	Sonic_HitWall
		tst.w	d1
		bpl.s	loc_1369A
		sub.w	d1,8(a0)
		move.w	#0,$10(a0)
		move.w	$12(a0),$14(a0)
		rts	
; ===========================================================================

loc_1369A:
		bsr.w	Sonic_DontRunOnWalls
		tst.w	d1
		bpl.s	loc_136B4
		sub.w	d1,$C(a0)
		tst.w	$12(a0)
		bpl.s	locret_136B2
		move.w	#0,$12(a0)

locret_136B2:
		rts	
; ===========================================================================

loc_136B4:
		tst.w	$12(a0)
		bmi.s	locret_136E0
		bsr.w	Sonic_HitFloor
		tst.w	d1
		bpl.s	locret_136E0
		add.w	d1,$C(a0)
		move.b	d3,$26(a0)
		bsr.w	Sonic_ResetOnFloor
		move.b	#0,$1C(a0)
		move.w	#0,$12(a0)
		move.w	$10(a0),$14(a0)

locret_136E0:
		rts	
; ===========================================================================

loc_136E2:
		bsr.w	Sonic_HitWall
		tst.w	d1
		bpl.s	loc_136F4
		sub.w	d1,8(a0)
		move.w	#0,$10(a0)

loc_136F4:
		bsr.w	sub_14EB4
		tst.w	d1
		bpl.s	loc_13706
		add.w	d1,8(a0)
		move.w	#0,$10(a0)

loc_13706:
		bsr.w	Sonic_DontRunOnWalls
		tst.w	d1
		bpl.s	locret_1373C
		sub.w	d1,$C(a0)
		move.b	d3,d0
		addi.b	#$20,d0
		andi.b	#$40,d0
		bne.s	loc_13726
		move.w	#0,$12(a0)
		rts	
; ===========================================================================

loc_13726:
		move.b	d3,$26(a0)
		bsr.w	Sonic_ResetOnFloor
		move.w	$12(a0),$14(a0)
		tst.b	d3
		bpl.s	locret_1373C
		neg.w	$14(a0)

locret_1373C:
		rts	
; ===========================================================================

loc_1373E:
		bsr.w	sub_14EB4
		tst.w	d1
		bpl.s	loc_13758
		add.w	d1,8(a0)
		move.w	#0,$10(a0)
		move.w	$12(a0),$14(a0)
		rts	
; ===========================================================================

loc_13758:
		bsr.w	Sonic_DontRunOnWalls
		tst.w	d1
		bpl.s	loc_13772
		sub.w	d1,$C(a0)
		tst.w	$12(a0)
		bpl.s	locret_13770
		move.w	#0,$12(a0)

locret_13770:
		rts	
; ===========================================================================

loc_13772:
		tst.w	$12(a0)
		bmi.s	locret_1379E
		bsr.w	Sonic_HitFloor
		tst.w	d1
		bpl.s	locret_1379E
		add.w	d1,$C(a0)
		move.b	d3,$26(a0)
		bsr.w	Sonic_ResetOnFloor
		move.b	#0,$1C(a0)
		move.w	#0,$12(a0)
		move.w	$10(a0),$14(a0)

locret_1379E:
		rts	
; End of function Sonic_Floor

; ---------------------------------------------------------------------------
; Subroutine to	reset Sonic's mode when he lands on the floor
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_ResetOnFloor:			; XREF: PlatformObject; et al
		btst	#4,$22(a0)
		beq.s	loc_137AE
		nop	
		nop	
		nop	

loc_137AE:
		bclr	#5,$22(a0)
		bclr	#1,$22(a0)
		bclr	#4,$22(a0)
		btst	#2,$22(a0)
		beq.s	loc_137E4
		bclr	#2,$22(a0)
		move.b	#$13,$16(a0)
		move.b	#9,$17(a0)
		move.b	#0,$1C(a0)	; use running/walking animation
		subq.w	#5,$C(a0)

loc_137E4:
		move.b	#0,$3C(a0)
		move.w	#0,($FFFFF7D0).w
		rts	
; End of function Sonic_ResetOnFloor

; ===========================================================================
; ---------------------------------------------------------------------------
; Sonic	when he	gets hurt
; ---------------------------------------------------------------------------

Obj01_Hurt:				; XREF: Obj01_Index
		jsr	SpeedToPos
		addi.w	#$30,$12(a0)
		btst	#6,$22(a0)
		beq.s	loc_1380C
		subi.w	#$20,$12(a0)

loc_1380C:
		bsr.w	Sonic_HurtStop
		bsr.w	Sonic_LevelBound
		bsr.w	Sonic_RecordPos
		bsr.w	Sonic_Animate
		bsr.w	LoadSonicDynPLC
		jmp	DisplaySprite

; ---------------------------------------------------------------------------
; Subroutine to	stop Sonic falling after he's been hurt
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_HurtStop:				; XREF: Obj01_Hurt
		move.w	($FFFFF72E).w,d0
		addi.w	#$E0,d0
		cmp.w	$C(a0),d0
		bcs.w	KillSonic
		bsr.w	Sonic_Floor
		btst	#1,$22(a0)
		bne.s	locret_13860
		moveq	#0,d0
		move.w	d0,$12(a0)
		move.w	d0,$10(a0)
		move.w	d0,$14(a0)
		move.b	#0,$1C(a0)
		subq.b	#2,$24(a0)
		move.w	#$78,$30(a0)

locret_13860:
		rts	
; End of function Sonic_HurtStop

; ===========================================================================
; ---------------------------------------------------------------------------
; Sonic	when he	dies
; ---------------------------------------------------------------------------

Obj01_Death:				; XREF: Obj01_Index
		bsr.w	GameOver
		jsr	ObjectFall
		bsr.w	Sonic_RecordPos
		bsr.w	Sonic_Animate
		bsr.w	LoadSonicDynPLC
		jmp	DisplaySprite

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


GameOver:				; XREF: Obj01_Death
		move.w	($FFFFF72E).w,d0
		addi.w	#$100,d0
		cmp.w	$C(a0),d0
		bcc.w	locret_13900
		move.w	#-$38,$12(a0)
		addq.b	#2,$24(a0)
		clr.b	($FFFFFE1E).w	; stop time counter
		addq.b	#1,($FFFFFE1C).w ; update lives	counter
		subq.b	#1,($FFFFFE12).w ; subtract 1 from number of lives
		bne.s	loc_138D4
		move.w	#0,$3A(a0)
		move.b	#$39,($FFFFD080).w ; load GAME object
		move.b	#$39,($FFFFD0C0).w ; load OVER object
		move.b	#1,($FFFFD0DA).w ; set OVER object to correct frame
		clr.b	($FFFFFE1A).w

loc_138C2:
		move.w	#$8F,d0
		jsr	(PlaySound).l	; play game over music
		moveq	#3,d0
		jmp	(LoadPLC).l	; load game over patterns
; ===========================================================================

loc_138D4:
		move.w	#60,$3A(a0)	; set time delay to 1 second
		tst.b	($FFFFFE1A).w	; is TIME OVER tag set?
		beq.s	locret_13900	; if not, branch
		move.w	#0,$3A(a0)
		move.b	#$39,($FFFFD080).w ; load TIME object
		move.b	#$39,($FFFFD0C0).w ; load OVER object
		move.b	#2,($FFFFD09A).w
		move.b	#3,($FFFFD0DA).w
		bra.s	loc_138C2
; ===========================================================================

locret_13900:
		rts	
; End of function GameOver

; ===========================================================================
; ---------------------------------------------------------------------------
; Sonic	when the level is restarted
; ---------------------------------------------------------------------------

Obj01_ResetLevel:			; XREF: Obj01_Index
		tst.w	$3A(a0)
		beq.s	locret_13914
		subq.w	#1,$3A(a0)	; subtract 1 from time delay
		bne.s	locret_13914
		move.w	#1,($FFFFFE02).w ; restart the level

locret_13914:
		rts	

; ---------------------------------------------------------------------------
; Subroutine to	make Sonic run around loops (GHZ/SLZ)
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Loops:				; XREF: Obj01_Control
		cmpi.b	#3,($FFFFFE10).w ; is level SLZ	?
		beq.s	loc_13926	; if yes, branch
		tst.b	($FFFFFE10).w	; is level GHZ ?
		bne.w	locret_139C2	; if not, branch

loc_13926:
		move.w	$C(a0),d0
		lsr.w	#1,d0
		andi.w	#$380,d0
		move.b	8(a0),d1
		andi.w	#$7F,d1
		add.w	d1,d0
		lea	($FFFFA400).w,a1
		move.b	(a1,d0.w),d1	; d1 is	the 256x256 tile Sonic is currently on
		cmp.b	($FFFFF7AE).w,d1
		beq.w	Obj01_ChkRoll
		cmp.b	($FFFFF7AF).w,d1
		beq.w	Obj01_ChkRoll
		cmp.b	($FFFFF7AC).w,d1
		beq.s	loc_13976
		cmp.b	($FFFFF7AD).w,d1
		beq.s	loc_13966
		bclr	#6,1(a0)
		rts	
; ===========================================================================

loc_13966:
		btst	#1,$22(a0)
		beq.s	loc_13976
		bclr	#6,1(a0)	; send Sonic to	high plane
		rts	
; ===========================================================================

loc_13976:
		move.w	8(a0),d2
		cmpi.b	#$2C,d2
		bcc.s	loc_13988
		bclr	#6,1(a0)	; send Sonic to	high plane
		rts	
; ===========================================================================

loc_13988:
		cmpi.b	#-$20,d2
		bcs.s	loc_13996
		bset	#6,1(a0)	; send Sonic to	low plane
		rts	
; ===========================================================================

loc_13996:
		btst	#6,1(a0)
		bne.s	loc_139B2
		move.b	$26(a0),d1
		beq.s	locret_139C2
		cmpi.b	#-$80,d1
		bhi.s	locret_139C2
		bset	#6,1(a0)	; send Sonic to	low plane
		rts	
; ===========================================================================

loc_139B2:
		move.b	$26(a0),d1
		cmpi.b	#-$80,d1
		bls.s	locret_139C2
		bclr	#6,1(a0)	; send Sonic to	high plane

locret_139C2:
		rts	
; End of function Sonic_Loops

; ---------------------------------------------------------------------------
; Subroutine to	animate	Sonic's sprites
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


Sonic_Animate:				; XREF: Obj01_Control; et al
		lea	(SonicAniData).l,a1
		moveq	#0,d0
		move.b	$1C(a0),d0
		cmp.b	$1D(a0),d0	; is animation set to restart?
		beq.s	SAnim_Do	; if not, branch
		move.b	d0,$1D(a0)	; set to "no restart"
		move.b	#0,$1B(a0)	; reset	animation
		move.b	#0,$1E(a0)	; reset	frame duration

SAnim_Do:
		add.w	d0,d0
		adda.w	(a1,d0.w),a1	; jump to appropriate animation	script
		move.b	(a1),d0
		bmi.s	SAnim_WalkRun	; if animation is walk/run/roll/jump, branch
		move.b	$22(a0),d1
		andi.b	#1,d1
		andi.b	#$FC,1(a0)
		or.b	d1,1(a0)
		subq.b	#1,$1E(a0)	; subtract 1 from frame	duration
		bpl.s	SAnim_Delay	; if time remains, branch
		move.b	d0,$1E(a0)	; load frame duration

SAnim_Do2:
		moveq	#0,d1
		move.b	$1B(a0),d1	; load current frame number
		move.b	1(a1,d1.w),d0	; read sprite number from script
		bmi.s	SAnim_End_FF	; if animation is complete, branch

SAnim_Next:
		move.b	d0,$1A(a0)	; load sprite number
		addq.b	#1,$1B(a0)	; next frame number

SAnim_Delay:
		rts	
; ===========================================================================

SAnim_End_FF:
		addq.b	#1,d0		; is the end flag = $FF	?
		bne.s	SAnim_End_FE	; if not, branch
		move.b	#0,$1B(a0)	; restart the animation
		move.b	1(a1),d0	; read sprite number
		bra.s	SAnim_Next
; ===========================================================================

SAnim_End_FE:
		addq.b	#1,d0		; is the end flag = $FE	?
		bne.s	SAnim_End_FD	; if not, branch
		move.b	2(a1,d1.w),d0	; read the next	byte in	the script
		sub.b	d0,$1B(a0)	; jump back d0 bytes in	the script
		sub.b	d0,d1
		move.b	1(a1,d1.w),d0	; read sprite number
		bra.s	SAnim_Next
; ===========================================================================

SAnim_End_FD:
		addq.b	#1,d0		; is the end flag = $FD	?
		bne.s	SAnim_End	; if not, branch
		move.b	2(a1,d1.w),$1C(a0) ; read next byte, run that animation

SAnim_End:
		rts	
; ===========================================================================

SAnim_WalkRun:				; XREF: SAnim_Do
		subq.b	#1,$1E(a0)	; subtract 1 from frame	duration
		bpl.s	SAnim_Delay	; if time remains, branch
		addq.b	#1,d0		; is animation walking/running?
		bne.w	SAnim_RollJump	; if not, branch
		moveq	#0,d1
		move.b	$26(a0),d0	; get Sonic's angle
		move.b	$22(a0),d2
		andi.b	#1,d2		; is Sonic mirrored horizontally?
		bne.s	loc_13A70	; if yes, branch
		not.b	d0		; reverse angle

loc_13A70:
		addi.b	#$10,d0		; add $10 to angle
		bpl.s	loc_13A78	; if angle is $0-$7F, branch
		moveq	#3,d1

loc_13A78:
		andi.b	#$FC,1(a0)
		eor.b	d1,d2
		or.b	d2,1(a0)
		btst	#5,$22(a0)
		bne.w	SAnim_Push
		lsr.b	#4,d0		; divide angle by $10
		andi.b	#6,d0		; angle	must be	0, 2, 4	or 6
		move.w	$14(a0),d2	; get Sonic's speed
		bpl.s	loc_13A9C
		neg.w	d2

loc_13A9C:
		lea (SonAni_3rdRun).l,a1
		cmpi.w #$A00,d2 ; is Sonic at super speed?
		bcc.s loc_13AB4 ; if yes, branch
		lea	(SonAni_Run).l,a1 ; use	running	animation
		cmpi.w	#$600,d2	; is Sonic at running speed?
		bcc.s	loc_13AB4	; if yes, branch
		lea	(SonAni_Walk).l,a1 ; use walking animation
		move.b	d0,d1
		lsr.b	#1,d1
		add.b	d1,d0

loc_13AB4:
		add.b	d0,d0
		move.b	d0,d3
		neg.w	d2
		addi.w	#$800,d2
		bpl.s	loc_13AC2
		moveq	#0,d2

loc_13AC2:
		lsr.w	#8,d2
		move.b	d2,$1E(a0)	; modify frame duration
		bsr.w	SAnim_Do2
		add.b	d3,$1A(a0)	; modify frame number
		rts	
; ===========================================================================

SAnim_RollJump:				; XREF: SAnim_WalkRun
		addq.b	#1,d0		; is animation rolling/jumping?
		bne.s	SAnim_Push	; if not, branch
		move.w	$14(a0),d2	; get Sonic's speed
		bpl.s	loc_13ADE
		neg.w	d2

loc_13ADE:
		lea	(SonAni_Roll2).l,a1 ; use fast animation
		cmpi.w	#$600,d2	; is Sonic moving fast?
		bcc.s	loc_13AF0	; if yes, branch
		lea	(SonAni_Roll).l,a1 ; use slower	animation

loc_13AF0:
		neg.w	d2
		addi.w	#$400,d2
		bpl.s	loc_13AFA
		moveq	#0,d2

loc_13AFA:
		lsr.w	#8,d2
		move.b	d2,$1E(a0)	; modify frame duration
		move.b	$22(a0),d1
		andi.b	#1,d1
		andi.b	#$FC,1(a0)
		or.b	d1,1(a0)
		bra.w	SAnim_Do2
; ===========================================================================

SAnim_Push:				; XREF: SAnim_RollJump
		move.w	$14(a0),d2	; get Sonic's speed
		bmi.s	loc_13B1E
		neg.w	d2

loc_13B1E:
		addi.w	#$800,d2
		bpl.s	loc_13B26
		moveq	#0,d2

loc_13B26:
		lsr.w	#6,d2
		move.b	d2,$1E(a0)	; modify frame duration
		lea	(SonAni_Push).l,a1
		move.b	$22(a0),d1
		andi.b	#1,d1
		andi.b	#$FC,1(a0)
		or.b	d1,1(a0)
		bra.w	SAnim_Do2
; End of function Sonic_Animate

; ===========================================================================
SonicAniData:
	include "_anim\Sonic.asm"

; ---------------------------------------------------------------------------
; Sonic	pattern	loading	subroutine
; ---------------------------------------------------------------------------

; ||||||||||||||| S U B	R O U T	I N E |||||||||||||||||||||||||||||||||||||||


LoadSonicDynPLC:			; XREF: Obj01_Control; et al
		moveq	#0,d0
		move.b	$1A(a0),d0	; load frame number
		cmp.b	($FFFFF766).w,d0
		beq.s	locret_13C96
		move.b	d0,($FFFFF766).w
		lea	(SonicDynPLC).l,a2
		add.w	d0,d0
		adda.w	(a2,d0.w),a2
		moveq	#0,d5
		move.b	(a2)+,d5
		subq.w	#1,d5
		bmi.s	locret_13C96
		move.w	#$F000,d4
		move.l	#Art_Sonic,d6

SPLC_ReadEntry:
		moveq	#0,d1
		move.b	(a2)+,d1
		lsl.w	#8,d1
		move.b	(a2)+,d1
		move.w	d1,d3
		lsr.w	#8,d3
		andi.w	#$F0,d3
		addi.w	#$10,d3
		andi.w	#$FFF,d1
		lsl.l	#5,d1
		add.l	d6,d1
		move.w	d4,d2
		add.w	d3,d4
		add.w	d3,d4
		jsr	(QueueDMATransfer).l
		dbf	d5,SPLC_ReadEntry	; repeat for number of entries
 
locret_13C96:
		rts	
; End of function LoadSonicDynPLC